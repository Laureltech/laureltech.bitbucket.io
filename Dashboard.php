<?php
include "Connection.php";
// Check user login or not
if(!isset($_SESSION['uname'])){
    header('Location: Login.php');
}
// logout
if(isset($_POST['but_logout'])){
    session_destroy();
    header('Location: Login.php');
}
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title> abierTTo Admin</title> 
    <link href="https://www.smarteyeapps.com/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.smarteyeapps.com/assets/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<style type="text/css">
.mbtable{
display:none;
}
.desktable{
  display:block;
}

div.card {
  width: 95%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;
  border-radius: 2rem;
}
div.card1 {
  width: 97%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;
  border-radius: 2rem;
}
div.header {
  background-color: #B2B2B2;

  color: black;
  padding: 8px;
  font-size: 14px;
border-top-right-radius: 2rem;
border-top-left-radius: 2rem;
}

div.container {
  padding: 30px;
}

.topnav {
  overflow: hidden;
  background-color: FBB040;
  height:50px;
}
.topnav1 {
  overflow: hidden;
 background-color:#DCDCDC;
  height:50px;
}
input[type="submit"] {
border: none;
outline:none;
background-color:transparent;
color:#444;
}
.topnav1 a {
    float: right;
  color: #444;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 14px;

  font-weight:bold;
}
.topnav1 a.active {
  color: #E31E36;
  font-weight:bold;
}
.div1{
   text-decoration:none; 
   color:black;
}
.content {
  padding: 16px;
  position: inherit;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 102px;
}
</style>
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
</head>
<body>
  <div class="desktable">
    <div class="topnav">
        <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
<div class="topnav1" id="myHeader">
  <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40" style="padding-top:10px ;padding-left:10px">
  <a href="#news"><form method='POST' action=""><input type="submit" value="Logout" name="but_logout" style="cursor:pointer"></form></a>
  <a href="Report.php">Report</a>
  <a href="History.php">History</a>
    <a  href="calculation.php">Calculation</a>
    <a  href="template.php">Create Template</a>
  <a href="AddEntry.php">Add Entry</a>
  <a class="active" href="Dashboard.php">Dashboard</a>
</div>
<div class="content">
  <h8 style="padding-left:10px"> Welcome Humberto! </h8>
  <table width="100%" style="vertical-align:top;font-family: 'Montserrat';font-size:14px">
    <tr>
      <td width="30%" style="vertical-align:top">
       
       <a href="calculation.php" style="text-decoration: none;color:black">
            <div class="card">
         <div class="header">
           <h3 style="font-size:15px;font-weight:bold;font-family: 'Montserrat';font-size:14px">Your wallet has</h3>
         </div>
  <div class="container">
   <?php
   // Make a MySQL Connection
   $result = mysqli_query($con, 'SELECT * FROM  points WHERE  MONTH(userDate) = MONTH(CURRENT_DATE());'); 
   $row = mysqli_fetch_assoc($result); 
   $curmonth = $row['Totalpoint'];
  ?>
    <p style="font-weight:500;font-size:19px"><span style="font-size:36px;font-weight:500;"> <?php echo $curmonth ;?></span> Puntos JunTTos</p>
  </div>
    </div>
    </a>
  </td>
  <td  width="30%" style="vertical-align:top">
  <a href="calculation.php" style="text-decoration: none;color:black">
  <div class="card">
  <div class="header">
    <h3 style="font-size:15px;font-weight:bold;font-family: 'Montserrat';font-size:14px">Value per point (as per month)</h3>
  </div>

  <div class="container">
  <?php
// Make a MySQL Connection
$result = mysqli_query($con, 'SELECT * FROM  points WHERE  userDate > (NOW() - INTERVAL 1 MONTH);'); 
$row = mysqli_fetch_assoc($result); 
$month = $row['Totalpoint'];
?>
  <p style="font-weight:500;font-size:22px">MXN<span style="font-size:36px;font-weight:500;padding-left:1px"> <?php echo $month ;?></span></p>
    <!-- <p><sub style="font-weight:bold">MXN</sub><span style="font-size:24px">90</span></p> -->
  </div>
</div></a>
  </td>
  <td width="40%" style="vertical-align:top"  rowspan="3">
    <div id="curve_chart1" style="width: 400px; height:150px ;margin-top:5%;margin-left:10%"></div>
    <div id="curve_chart" style="width: 400px; height:150px ;margin-top:5%;margin-left:10%"></div>
  </td>
  </tr>
  <tr>
  <td colspan="2">
      <a href="calculation.php" style="text-decoration: none;color:black">
  <div class="card1">
  <div class="header">
    <h3 style="font-size:15px;font-weight:bold">This means your accrued value in Puntos JunTTos is worth</h3>
  </div>

  <div class="container">
  <?php
// Make a MySQL Connection
$result = mysqli_query($con, 'SELECT * FROM  points WHERE  userDate > (NOW() - INTERVAL 1 MONTH);'); 
$result2 = mysqli_query($con, 'SELECT * FROM  points WHERE  MONTH(userDate) = MONTH(CURRENT_DATE());'); 
$row = mysqli_fetch_assoc($result); 
$row1 = mysqli_fetch_assoc($result2); 
$curmonth = $row['Totalpoint'] * $row1['Totalpoint'] ;
?>
  <p style="font-weight:500;font-size:22px">MXN<span style="font-size:36px;font-weight:500;padding-left:1px"> <?php echo $curmonth ;?></span></p>
  </div>
</div></a>
  </td>
  </tr>
  </table>
</div>
</div>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>


<!-- Mobile View -->
<style>
@media only screen and (max-width: 600px) {
  .mbtable{
display:block;
}
.desktable{
  display:none;
}
  div.mcard {
  width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;

}
.topnav {
  overflow: hidden;
  background-color: FBB040;
  height:50px;
}

body { margin: 0 auto; }

header {
	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;
}
footer {
	height: 50px;
	background-color: #494949;
	color: #fff;
	display: flex;
	align-items: center;
	justify-content: center;
	bottom: 0;
	position: fixed;
	width: 100%;
}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
.topnav1 {
  overflow: hidden;
  background-color:#B2B2B2;
  height:50px;
}
.topnav1 a {
  color: #444;
  /* text-align: center; */
  padding: 14px 16px;
  text-decoration: none;
  font-size: 10px;
}
.topnav1 a.active {
  color: #E31E36;
  font-weight:bold;
}
div.card1 {
  width: 95%;
  text-align: center;
  margin-left:10px;
  margin-top:20px;
border-radius: 2rem;
}
@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;
}
body { margin: 0 auto; }

header {
	height: 60px;
  background-color:B2B2B2;

	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;

}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
  .topnav a:not(:first-child), .dropdown .dropbtn {
  
  }
  .topnav1 a.icon {
    float: right;
    display: block;
  }
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

}

@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
  border-radius: 2rem;

}
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

  .topnav1.responsive {position: relative;}
  .topnav1.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav1.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav1.responsive .dropdown {float: none;}
  .topnav1.responsive .dropdown-content {position: relative;}
  .topnav1.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}
}
.logout{
    background: transparent;
    text-transform: uppercase;
    padding-left:1px;
    cursor: pointer;
}

 </style>


 <div class="mbtable">
 <div class="topnav">
   <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
 <header>
 
		<div class="top">
    <table>
    <tr>
    <td>
        <a href="#" style=" float: right;" class="menu_icon"><i style=" float: left;"  class="material-icons">dehaze</i></a>

    </td>
    <td style="padding-right:15px">
        <!-- <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40" style="padding-top:10px ;padding-left:10px">-->
        <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40"  style="margin-left:60%">


    </td>
    </tr>
    </table>

		</div>
	</header>
	<nav class="menu">
  <a  class="item_menu" href="Dashboard.php">Dashboard</a>
  <a  class="item_menu" href="AddEntry.php">Add Entry</a>
  <a  class="item_menu" href="History.php">History</a>
  <a  class="item_menu" class="active" href="Report.php">Report</a>
  <a href="#"  class="item_menu" style="cursor: pointer;">
  <form method='post' action="" style="cursor: pointer;">
            <input type="submit" value="Logout" style="cursor: pointer;" class="logout" name="but_logout">
        </form>
  </a>
	</nav>
	<main>
	    <a href="calculation.php" style="text-decoration: none;color:black">
  <div class="mcard">
  <div class="header">
    <h4><strong>Your wallet has</strong></h4>
  </div>

  <div class="container">
  <?php
// Make a MySQL Connection


$result = mysqli_query($con, 'SELECT * FROM  points WHERE  MONTH(userDate) = MONTH(CURRENT_DATE());'); 
$row = mysqli_fetch_assoc($result); 
$curmonth = $row['Totalpoint'];


?>
    <p style="font-weight:500;font-size:19px"><span style="font-size:36px;font-weight:500;"> <?php echo $curmonth ;?></span> Puntos JunTTos</p>
  </div>
</div>
</a>
<a href="calculation.php" style="text-decoration: none;color:black">
<div class="mcard">
<div class="header">
    <h4><strong>Value per point (as per month)</strong></h4>
  </div>

  <div class="container">
  <?php
// Make a MySQL Connection
$result = mysqli_query($con, 'SELECT * FROM  points WHERE  userDate > (NOW() - INTERVAL 1 MONTH);'); 
$row = mysqli_fetch_assoc($result); 
$month = $row['Totalpoint'];
?>
  <p style="font-weight:500;font-size:22px">MXN<span style="font-size:36px;font-weight:500;padding-left:1px"> <?php echo $month ;?></span></p>
    <!-- <p><sub style="font-weight:bold">MXN</sub><span style="font-size:24px">90</span></p> -->
  </div>
</div>
</a>
 <a href="calculation.php" style="text-decoration: none;color:black">
<div class="mcard">
<div class="header">
    <h4><strong>This means your accrued value in Puntos JunTTos is worth</strong></h4>
  </div>

  <div class="container">
  <?php
// Make a MySQL Connection
$result = mysqli_query($con, 'SELECT * FROM  points WHERE  userDate > (NOW() - INTERVAL 1 MONTH);'); 
$result2 = mysqli_query($con, 'SELECT * FROM  points WHERE  MONTH(userDate) = MONTH(CURRENT_DATE());'); 
$row = mysqli_fetch_assoc($result); 
$row1 = mysqli_fetch_assoc($result2); 
$curmonth = $row['Totalpoint'] * $row1['Totalpoint'] ;
?>
  <p style="font-weight:500;font-size:22px">MXN<span style="font-size:36px;font-weight:500;padding-left:1px"> <?php echo $curmonth ;?></span></p>
  </div>
</div></a>

<div class="mcard">
<div id="mcurve_chart" style="width:100%; height:40% ;margin-top:5%;"></div>
</div>
<div class="mcard">
<div id="m1curve_chart" style="width:100%; height:40% ;margin-top:5%;margin-bottom:5%;"></div>
</div>
	</main>
</div>
 </body>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Total worth per loyalty point'],

          <?php
           $query="SELECT Time,AVG(totalworth) as Totalpoint FROM calculation GROUP BY Month(Time)";
          $res=mysqli_query($con,$query);
          while($data=mysqli_fetch_array($res)){
            $year= date_format(date_create( $data['Time'])," M");
            $sale=$data['Totalpoint'];
             
          
          ?>
            ['<?php echo $year;?>',<?php echo $sale;?>], 
          <?php      
              }
          ?> 
         
        ]);

        var options = {
          title: '',
          curveType: 'function',
        legend: { position: 'bottom' }
          
        };

        var chart = new google.visualization.LineChart(document.getElementById('mcurve_chart'));

        chart.draw(data, options);
      }
    </script>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Accurued total points generated in the system'],

          <?php
          $query="SELECT Time,AVG(Referrals) as Totalpoint FROM calculation GROUP BY Month(Time)";
          $res=mysqli_query($con,$query);
          while($data=mysqli_fetch_array($res)){
            $year= date_format(date_create( $data['Time']),"M");
            $sale=$data['Totalpoint'];
             
          
          ?>
            ['<?php echo $year;?>',<?php echo $sale;?>], 
          <?php      
              }
          ?> 
         
        ]);

        var options = {
          title: '',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('m1curve_chart'));

        chart.draw(data, options);
      }
    </script>
 
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Accurued total points generated in the system'],

          <?php
          $query="SELECT Time,AVG(Referrals) as Totalpoint FROM calculation GROUP BY Month(Time)";

          $res=mysqli_query($con,$query);
          while($data=mysqli_fetch_array($res)){
            $year= date_format(date_create( $data['Time']),"M");
            $sale=$data['Totalpoint'];
             
          
          ?>
            ['<?php echo $year;?>',<?php echo $sale;?>], 
          <?php      
              }
          ?> 
         
        ]);

        var options = {
          title: '',
          curveType: 'function',
          legend: { position: 'bottom' }
    //       vAxis: {
    // gridlines: {
    //     color: 'transparent'
    // }
// }
          
      };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
    
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Total worth per loyalty point'],

          <?php
           $query="SELECT Time,AVG(totalworth) as Totalpoint FROM calculation GROUP BY Month(Time)";
         $res=mysqli_query($con,$query);
          while($data=mysqli_fetch_array($res)){
            $year= date_format(date_create( $data['Time'])," M");
            $sale=$data['Totalpoint'];
             
          
          ?>
            ['<?php echo $year;?>',<?php echo $sale;?>], 
          <?php      
              }
          ?> 
         
        ]);

 var options = {
          title: '',
          curveType: 'function',
          legend: { position: 'bottom' }
    //       vAxis: {
    // gridlines: {
    //     color: 'transparent'
    // }
// }
          
      };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart1'));

        chart.draw(data, options);
      }
    </script>
      <script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
</html>