
<?php
// Load the database configuration file
include 'Connection.php';

$filename = "abierTTo_Report" . date('Y-m-d') . ".csv";
$delimiter = ",";
$from = $_GET['from'];
$to = $_GET['to'];
$search = $_GET['search'];
// Create a file pointer
echo "search: ".$search;

$f = fopen('php://memory', 'w');

// Set column headers
$fields = array('ID', 
'userDate', 
'userName',
 'userType',
  'Totalqty', 
  'Totalpoint',
  'clientReferralsqty',
  'clientReferralspoints',
  'clientpurchasecreditqty',
  'clientpurchasecreditpoints',
  'clientpurchasecashqty',
  'clientpurchasecashpoints',
  'clientpurchasedifferqty',
  'clientpurchasedifferpoints',
  'clientpurchasedeachqty',
  'clientpurchasedeachpoints',
  'businessReferralsqty',
  'businessReferralspoints',
  'businessPurchasedMonthlyqty',
  'businessPurchasedMonthlypoints',
  'businessPurchasedHalfqty',
  'businessPurchasedHalfpoints',
  'businessPurchasedyearqty',
  'businessPurchasedyearpoints',
  'businessSoldqty',
  'businessSoldpoints',
'businessSaleMonthqty',
'businessSaleMonthpoints',
'businessSoldCreditqty',
'businessSoldCreditpoints',
'businessPurchasedPushqty',
'businessPurchasedPushpoints',
'businessPurchasedPushNoteqty',
'businessPurchasedPushNotepoints',
'deliveryReferralsqty',
'deliveryReferralspoints',
'deliveryItemqty',
'deliveryItempoints',
'deliverycompletedqty',
'deliverycompletedpoints'
);

fputcsv($f, $fields, $delimiter);
// Get records from the database
  
if($from!="" && $to!="" && $search!="")
{
  $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to' AND (userName='$search' OR userType LiKe'%$search%' OR ID='$search' OR Totalpoint='$search' OR Totalqty='$search')");

}else
if($from!="" && $to!="" ){
 $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to'");
}else if($search!="")
{
    $result = mysqli_query($con,"SELECT * FROM points where  userName='$search'  OR userType LiKe'%$search%' OR ID='$search'OR Totalpoint='$search' OR Totalqty='$search'");

}
else{
$result = mysqli_query($con,"SELECT * FROM points ORDER BY userDate  DESC");
}
if(mysqli_num_rows($result) > 0){
// Output each row of the data, format line as csv and write to file pointer
while($row = mysqli_fetch_assoc($result)){
$lineData = array($row['ID'], 
$row['userDate'], 
$row['userName'], 
$row['userType'], 
$row['Totalqty'], 
$row['Totalpoint'], 
$row['clientReferralsqty'], 
$row['clientReferralspoints'], 
$row['clientpurchasecreditqty'], 
$row['clientpurchasecreditpoints'],
$row['clientpurchasecashqty'],
$row['clientpurchasecashpoints'],
$row['clientpurchasedifferqty'],
$row['clientpurchasedifferpoints'],
$row['clientpurchasedeachqty'],
$row['clientpurchasedeachpoints'],
$row['businessReferralsqty'],
$row['businessReferralspoints'],
$row['businessPurchasedMonthlyqty'],
$row['businessPurchasedMonthlypoints'],
$row['businessPurchasedHalfqty'],
$row['businessPurchasedHalfpoints'],
$row['businessPurchasedyearqty'],
$row['businessPurchasedyearpoints'],
$row['businessSoldqty'],
$row['businessSoldpoints'],
$row['businessSaleMonthqty'],
$row['businessSaleMonthpoints'],
$row['businessSoldCreditqty'],
$row['businessSoldCreditpoints'],
$row['businessPurchasedPushqty'],
$row['businessPurchasedPushpoints'],
$row['businessPurchasedPushNoteqty'],
$row['businessPurchasedPushNotepoints'],
$row['deliveryReferralsqty'],
$row['deliveryReferralspoints'],
$row['deliveryItemqty'],
$row['deliveryItempoints'],
$row['deliverycompletedqty'],
$row['deliverycompletedpoints'],
);

fputcsv($f, $lineData, $delimiter);
}
}

// Move back to beginning of file
fseek($f, 0);

// Set headers to download file rather than displayed
header('Content-Type:text/csv');
header('Content-Disposition:attachment;filename="'.$filename.'";');

// Output all remaining data on a file pointer
fpassthru($f);

// Exit from file
exit();
