<?php
include "Connection.php";

// Check user login or not
if(!isset($_SESSION['uname'])){
    header('Location: Login.php');
}

// logout
if(isset($_POST['but_logout'])){
    session_destroy();
    header('Location: Login.php');
}
?>

<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <title> abierTTo Admin</title> 
      <link href="https://www.smarteyeapps.com/assets/css/bootstrap.min.css" rel="stylesheet">
     <link href="https://www.smarteyeapps.com/assets/css/font-awesome.min.css" rel="stylesheet">
     <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<style type="text/css">
div.card {
  width: 95%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;

}

div.header {
  background-color: #f2f2f1;
  color: #444;
  padding: 8px;
  font-size: 10px;
}

div.container {
  padding: 30px;
}
:root {
  --input-padding-x: 1.5rem;
  --input-padding-y: 0.75rem;
}

.login,
.image {
  min-height: 90vh;
}

.bg-image {
  background-image: url('./Images/people-3245739_1280-700x687.png');
  min-height: 100%;
}

.login-heading {
  font-weight: 300;
}

.btn-login {
  font-size: 0.9rem;
  letter-spacing: 0.05rem;
  padding: 0.75rem 1rem;
  border-radius: 2rem;
}

.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}

.form-label-group>input,
.form-label-group>label {
  padding: var(--input-padding-y) var(--input-padding-x);
  height: auto;
  border-radius: 2rem;
}

.form-label-group>label {
  position: absolute;
  top: 0;
  left: 0;
  display: block;
  width: 100%;
  margin-bottom: 0;
  /* Override default `<label>` margin */
  line-height: 1.5;
  color: #495057;
  cursor: text;
  /* Match the input under the label */
  border: 1px solid transparent;
  border-radius: .25rem;
  transition: all .1s ease-in-out;
}

.form-label-group input::-webkit-input-placeholder {
  color: transparent;
}

.form-label-group input:-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-ms-input-placeholder {
  color: transparent;
}

.form-label-group input::-moz-placeholder {
  color: transparent;
}

.form-label-group input::placeholder {
  color: transparent;
}

.form-label-group input:not(:placeholder-shown) {
  padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
  padding-bottom: calc(var(--input-padding-y) / 3);
}

.form-label-group input:not(:placeholder-shown)~label {
  padding-top: calc(var(--input-padding-y) / 3);
  padding-bottom: calc(var(--input-padding-y) / 3);
  font-size: 12px;
  color: #777;
}

/* Fallback for Edge
-------------------------------------------------- */

@supports (-ms-ime-align: auto) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input::-ms-input-placeholder {
    color: #777;
  }
}

/* Fallback for IE
-------------------------------------------------- */

@media all and (-ms-high-contrast: none),
(-ms-high-contrast: active) {
  .form-label-group>label {
    display: none;
  }
  .form-label-group input:-ms-input-placeholder {
    color: #777;
  }
}

.topnav {
  overflow: hidden;
  background-color: rgb(232,172,75);
  height:50px;
}
.topnav1 {
  overflow: hidden;
  background-color:rgba(242,242,242,255);
  height:50px;
}
input[type="submit"] {
border: none;
outline:none;
}
.topnav1 a {
    float: right;
  color: #444;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

/* .topnav1 a:hover {
  background-color: #ddd;
  color: black;
} */

.topnav1 a.active {
  color: #d27c85;
  font-weight:bold;
}
</style>
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    </head>
    <body>
    <div class="topnav">
    <!-- <img src="./Images/abier.png" alt="Girl in a jacket" width="300" height="50"> -->
</div>
<div class="topnav1">
    <img src="./Images/abier.png" alt="Girl in a jacket" width="200" height="50">
  <a href="#news">
  <form method='post' action="">
            <input type="submit" value="Logout" name="but_logout">
        </form>
  </a>
  <a href="Report.php">Report</a>
  <a href="History.php">History</a>
  <a href="AddEntry.php">Add Entry</a>
  <a class="active" href="Dashboard.php">Dashboard</a>
</div>
  
<!DOCTYPE html>
<html>
  <body>
	<form method="post" action="process.php">
		First name:<br>
		<input type="text" name="first_name">
		<br>
		Last name:<br>
		<input type="text" name="last_name">
		<br>
		City name:<br>
		<input type="text" name="city_name">
		<br>
		Email Id:<br>
		<input type="email" name="email">
		<br><br>
		<input type="submit" name="save" value="submit">
	</form>
  </body>
</html>
</body></html>