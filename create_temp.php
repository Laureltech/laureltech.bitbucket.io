<?php
include "Connection.php";

// Check user login or not
if(!isset($_SESSION['uname'])){
    header('Location: Login.php');
}

// logout
if(isset($_POST['but_logout'])){
    session_destroy();
    header('Location: Login.php');
}

if(isset($_POST['save']))
{	 
	 $Name = ucfirst($_POST['Name']);
	 $Refclient = $_POST['Refclient'];
	 $Refclientprice = $_POST['Refclientprice'];
	 $Refclientpoint = $_POST['Refclientpoint'];

	 $Refbusiness = $_POST['Refbusiness'];
	 $Refbusinesspoint = $_POST['Refbusinesspoint'];
	 $Refbusinessprice = $_POST['Refbusinessprice'];

	 $busAMBA = $_POST['busAMBA'];
	 $busAMBApoint= $_POST['busAMBApoint'];
	 $busAMBAprice= $_POST['busAMBAprice'];

	  $buspusha = $_POST['buspusha'];
	 $buspushapoint= $_POST['buspushapoint'];
	 	 $buspushaprice= $_POST['buspushaprice'];

	   $buspushb = $_POST['buspushb'];
	 $buspushbpoint= $_POST['buspushbpoint'];
	 	 $buspushbprice= $_POST['buspushbprice'];

	   $buspushc = $_POST['buspushc'];
	 $buspushcpoint= $_POST['buspushcpoint'];
	 	 $buspushcprice= $_POST['buspushcprice'];

	 
	 $RefDelivery = $_POST['RefDelivery'];
	  $RefDeliverypoint = $_POST['RefDeliverypoint'];
	 	 $RefDeliveryprice = $_POST['RefDeliveryprice'];
	 	 
	 $clientCredit = $_POST['clientCredit'];
	 $clientCreditpoint = $_POST['clientCreditpoint'];
	 	 $clientCreditprice = $_POST['clientCreditprice'];
	
	 $clientCash = $_POST['clientCash'];
	 $clientCashpoint = $_POST['clientCashpoint'];
	 	 $clientCashprice = $_POST['clientCashprice']; 
	 	 
	 $client5busi = $_POST['client5busi'];
	 $client5busipoint = $_POST['client5busipoint'];
	 	 $client5busiprice = $_POST['client5busiprice'];
	 	 
	 $client5each = $_POST['client5each'];
	 $client5eachpoint= $_POST['client5eachpoint'];
	 	 $client5eachprice= $_POST['client5eachprice'];
	 	 
	 $busimonth= $_POST['busimonth'];
	 	 $busimonthpoint = $_POST['busimonthpoint'];
	 	 $busimonthprice= $_POST['busimonthprice'];
	 	 
	 $busihalfyear = $_POST['busihalfyear'];
	 	 $busihalfyearpoint = $_POST['busihalfyearpoint'];
	 	 $busihalfyearprice = $_POST['busihalfyearprice'];
	 	 
	 $busiyear = $_POST['busiyear'];
	 $busiyearpoint = $_POST['busiyearpoint'];
	 	 $busiyearprice = $_POST['busiyearprice'];
	 	 
	 $busi5differ = $_POST['busi5differ'];
	 	 $busi5differpoint = $_POST['busi5differpoint'];
	 	 $busi5differprice = $_POST['busi5differprice'];
	
	 $busi10sale = $_POST['busi10sale'];
	  $busi10salepoint= $_POST['busi10salepoint'];
	 	 $busi10saleprice= $_POST['busi10saleprice'];
	 	 
	 $busisold10 = $_POST['busisold10'];
	 $busisold10point = $_POST['busisold10point'];
	 $busisold10price = $_POST['busisold10price'];
	 	 
	 $busipurpush = $_POST['busipurpush'];
	 	 $busipurpushpoint = $_POST['busipurpushpoint'];
	 	 $busipurpushprice = $_POST['busipurpushprice'];
	 	 
	 $busipur5push = $_POST['busipur5push'];
	 	 $busipur5pushpoint = $_POST['busipur5pushpoint'];
	 	 $busipur5pushprice = $_POST['busipur5pushprice'];
	 	 
	 $deliveryitem = $_POST['deliveryitem'];
	 	 $deliveryitempoint = $_POST['deliveryitempoint'];
	 	 $deliveryitemprice = $_POST['deliveryitemprice'];
	 	 
	 $delivery5complete = $_POST['delivery5complete'];
	  $delivery5completepoint = $_POST['delivery5completepoint'];
	 	 $delivery5completeprice = $_POST['delivery5completeprice'];
	 	 
	 $extaction = $_POST['extaction'];
	  $extpoint = $_POST['extpoint'];
	 	 $extprice = $_POST['extprice'];
	

	 	 
  $sql = "INSERT INTO template (
  Name,
Refclient,
Refbusiness,
RefDelivery,
clientCredit,
clientCash,
	client5busi,
		client5each,
busimonth,
busihalfyear,
busiyear,
busi5differ,
busi10sale,
busisold10,
busipurpush,
busipur5push,
deliveryitem,
delivery5complete,
Refclientpoint,
Refbusinesspoint,
RefDeliverypoint,
clientCreditpoint,
clientCashpoint,
client5busipoint,
client5eachpoint,
busimonthpoint,
busihalfyearpoint,
busiyearpoint,
busi5differpoint,
busi10salepoint,
busisold10point,
busipurpushpoint,
busipur5pushpoint,
busAMBA,
busAMBApoint,
buspusha,
buspushapoint,
buspushb,
buspushbpoint,
buspushc,
buspushcpoint,
deliveryitempoint,
delivery5completepoint,
Refclientprice,
Refbusinessprice,
RefDeliveryprice,
clientCreditprice,
clientCashprice,
client5busiprice,
client5eachprice,
busimonthprice,
busihalfyearprice,
busiyearprice,
busi5differprice,
busi10saleprice,
busisold10price,
busipurpushprice,
busipur5pushprice,
deliveryitemprice,
delivery5completeprice,
busAMBAprice,
	buspushaprice,
	buspushbprice,
	buspushcprice,
	extaction,
	extpoint,
	extprice
)
VALUES (
    '$Name',
    '$Refclient',
    '$Refbusiness',
    '$RefDelivery',
     '$clientCredit',
     '$clientCash',
      '$client5busi',
       '$client5each',
 '$busimonth',
  '$busihalfyear',
    '$busiyear',
    '$busi5differ',
    '$busi10sale',
    '$busisold10',
    '$busipurpush',
    '$busipur5push',
    '$deliveryitem',
    '$delivery5complete',
    '$Refclientpoint',
    '$Refbusinesspoint',
    '$RefDeliverypoint',
    '$clientCreditpoint',
    '$clientCashpoint',
    '$client5busipoint',
    '$client5eachpoint',
    '$busimonthpoint',
    '$busihalfyearpoint',
    '$busiyearpoint',
    '$busi5differpoint',
    '$busi10salepoint',
    '$busisold10point',
    '$busipurpushpoint',
    '$busipur5pushpoint',
    '$busAMBA',
    '$busAMBApoint',
    '$buspusha',
    '$buspushapoint',
    '$buspushb',
    '$buspushbpoint',
    '$buspushc',
    '$buspushcpoint',
    '$deliveryitempoint',
    '$delivery5completepoint',
    '$Refclientprice',
    '$Refbusinessprice',
    '$RefDeliveryprice',
    '$clientCreditprice',
    '$clientCashprice',
    '$client5busiprice',
    '$client5eachprice',
    '$busimonthprice',
    '$busihalfyearprice',
    '$busiyearprice',
    '$busi5differprice',
    '$busi10saleprice',
    '$busisold10price',
    '$busipurpushprice',
    '$busipur5pushprice',
    '$deliveryitemprice',
    '$delivery5completeprice',
    '$busAMBAprice',
    '$buspushaprice',
    '$buspushbprice',
    '$buspushcprice',
    '$extaction',
    '$extpoint',
    '$extprice'
	
   )";
if (mysqli_query($con, $sql)) {
  // alert( "");
  echo '<script>alert("New record created successfully !")</script>';
              header('Location:template.php');


	 } else {
		echo "Error: " . $sql . "
" . mysqli_error($con);
	 }
	 mysqli_close($con);
}
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> abierTTo Admin</title> 
    <link href="https://www.smarteyeapps.com/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.smarteyeapps.com/assets/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<style type="text/css">
.mbtable{
display:none;
}
.desktable{
  display:block;
}
input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
  margin-left:auto;
  margin-right:auto;
  border: 1px solid #D3D3D3;
}
input[type=text]:focus {
    outline: none;
}

.container1 {
  display: block;
  position: relative;
  padding-left: 25px;
  padding-top:5px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;

  user-select: none;
}

/* Hide the browser's default checkbox */
.container1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  
}

/* Create a custom checkbox */
.checkmark {
    margin-top:5px;
  position: absolute;
  top: 0;
  left: 0;
  height: 18px;
  width: 18px;
  border: solid 1px  #2196F3;
 border-radius: 50%;
}



/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}
textarea {
  width: 100%;
  height: auto;
  vertical-align:middle;
  box-sizing: border-box;
  border: 1px solid #ccc;
  font-size: 14px;
  resize: none;
}
/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
  left: 4px;
  top: 2px;
  width: 7px;
  height: 11px;
  border: .5px solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
div.card {
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  
}
div.card1 {
  width: 97%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;
}
textarea {
  width: 100%;
  height: auto;
  padding: 12px 20px;
  box-sizing: border-box;
  border: 1px solid #ccc;
  resize: none;
}
.search{
  background-color: #5CC4BA;
 border-radius: 2rem; 
  border: none;
  color: white;
  padding-top:10px;
  padding-bottom:10px;
  /*padding-left:60px;*/
  /*padding-right:60px;*/
  text-decoration: none;
  margin: 4px 2px;
  font-weight:bold;
  cursor: pointer;
}
div.header {
  background-color: #B2B2B2;
  color: black;
  padding: 8px;
  font-weight: bold;

}

div.container {
  padding: 0px;
  font-size: 2em;
}

.topnav {
  overflow: hidden;
  background-color: #FBB040;
  height:50px;
}
.topnav1 {
  overflow: hidden;
 background-color:#DCDCDC;
  height:50px;
}
input[type="submit"] {
border: none;
outline:none;
}
.topnav1 a {
    float: right;
  color: #444;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 14px;

  font-weight:bold;
}
.btnDelete {
  background-color: #5CC4BA;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 14px;
  cursor: pointer;
  margin-top:10px;
  float:right;
}

/* Darker background on mouse-over */
.btnDelete:hover {
  background-color: #5CC4BA;
}
.topnav1 a.active {
  color: #E31E36;
  font-weight:bold;
}
.input-symbol-euro {
    position: relative;
}
.input-symbol-euro input {
    padding-left:18px;
}
.input-symbol-euro:before {
    position: absolute;
    top: 0;
    content:"$";
    left: 15px;
}
.input-symbol-euro1:before {
    position: absolute;
    top: 0;
    content:"$";
    left: 15px;
}

.label{
  margin: 10px;
}
.row1{
    border-bottom: 1px solid #E8E8E8;
    height:60px;
    font-size:14px;
}
.close {
  position: absolute;
  right: 32px;
  top: 32px;
  width: 32px;
  height: 32px;
  opacity: 0.3;
}
.close:hover {
  opacity: 1;
}
.close:before, .close:after {
  position: absolute;
  left: 15px;
  content: ' ';
  height: 33px;
  width: 2px;
  background-color: #333;
}
.close:before {
  transform: rotate(45deg);
}
.close:after {
  transform: rotate(-45deg);
}
.content {
  position: inherit;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
   padding-top: 10px;
}
.float-container {
  border: solid 1px #ccc;
  display:flex;
}
.float-container input {
  border: none;
  outline: 0;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
      <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    </head>
    <body>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
 
        function Add() {
            AddRow($("#txtAction").val(), $("#txtpoint").val(), $("#txtprice").val());
            $("#txtAction").val("");
            
            $("#txtpoint").val("");
            $("#txtprice").val("");
        };
 
        function AddRow(extaction, extpoint,extprice) {
            //Get the reference of the Table's TBODY element.
            var tBody = $("#tblCustomers > TBODY")[0];
 
            //Add Row.
            row = tBody.insertRow(-1);
 
            //Add Name cell.
            var cell = $(row.insertCell(-1));
    var addaction = $("<input />");
            addaction.attr("type", "text");
              addaction.attr("name", "extaction");
            addaction.val(extaction);
            cell.append(addaction);
            //Add Country cell.
            cell = $(row.insertCell(-1));
              var addpoint = $("<input />");
            addpoint.attr("type", "text");
                        addpoint.attr("name", "extpoint");
            addpoint.attr("style", "width:100%;display:block;margin-left:auto;margin-right:auto");
            addpoint.val(extpoint);
            
            cell.append(addpoint);
      //Add Country cell.
            cell = $(row.insertCell(-1));
            
             var addprice = $("<input />");
            addprice.attr("type", "text");
              addprice.attr("name", "extprice");
              addprice.attr("style", "width:100%;display:block;margin-left:auto;margin-right:auto;");
              
            addprice.val(extprice);
            
            cell.append(addprice);
            //Add Button cell.
            cell = $(row.insertCell(-1));
            var btnRemove = $("<input />");
            btnRemove.attr("type", "button");
             btnRemove.attr("style", " width:100%;display:block;margin-left:auto;margin-right:auto");
 btnRemove.attr("class", "btnDelete");
 
            btnRemove.attr("onclick", "Remove(this);");
            btnRemove.val("Remove");
            cell.append(btnRemove);
            
        };
 
        function Remove(button) {
            //Determine the reference of the Row using the Button.
            var row = $(button).closest("TR");
            var name = $("TD", row).eq(0).html();
            if (confirm("Do you want to delete! ")) {
 
                //Get the reference of the Table.
                var table = $("#tblCustomers")[0];
 
                //Delete the Table row using it's Index.
                table.deleteRow(row[0].rowIndex);
            }
        };
    </script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" /></script>

  <script>
     $(document).ready(function () {
            //iterate through each textboxes and add keyup
            //handler to trigger sum event
            $(".txt").each(function () {
                $(this).keyup(function () {
                    calculateSum();
                });
            });
        });

        function calculateSum() {
            var sum = 0;
            //iterate through each textboxes and add the values
            $(".txt").each(function () {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                }
            });
            
            var sumQ = [];
            for (var i=1; i<=3; i++) {
                sumQ[i] = 0;
                $('td:nth-child('+(i+1)+')').find(".txt").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sumQ[i] += parseFloat(this.value);
                    }
                });
                $(".span7").find('input').eq(i-1).val(sumQ[i]);
            }

            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum").val(sum);
              var sum1 = 0;
            //iterate through each textboxes and add the values
            $(".txt1").each(function () {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum1 += parseFloat(this.value);
                }
            });
            
            var sumQ1 = [];
            for (var i=1; i<=3; i++) {
                sumQ1[i] = 0;
                $('td:nth-child('+(i+1)+')').find(".txt1").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sumQ1[i] += parseFloat(this.value);
                    }
                });
                $(".span7").find('input1').eq(i-1).val(sumQ1[i]);
            }

            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum1").val(sum1);
        }
  </script>
   <script>
     $(document).ready(function () {
            //iterate through each textboxes and add keyup
            //handler to trigger sum event
            $(".txt2").each(function () {
                $(this).keyup(function () {
                    calculateSum1();
                });
            });
        });

        function calculateSum1() {
            var sum2 = 0;
            //iterate through each textboxes and add the values
            $(".txt2").each(function () {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum2 += parseFloat(this.value);
                }
            });
            
            var sumQ2 = [];
            for (var i=1; i<=3; i++) {
                sumQ2[i] = 0;
                $('td:nth-child('+(i+1)+')').find(".txt2").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sumQ2[i] += parseFloat(this.value);
                    }
                });
                $(".span7").find('input2').eq(i-1).val(sumQ2[i]);
            }

            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum2").val(sum2);
              var sum3 = 0;
            //iterate through each textboxes and add the values
            $(".txt3").each(function () {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum3 += parseFloat(this.value);
                }
            });
            
            var sumQ3 = [];
            for (var i=1; i<=3; i++) {
                sumQ3[i] = 0;
                $('td:nth-child('+(i+1)+')').find(".txt3").each(function () {
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sumQ3[i] += parseFloat(this.value);
                    }
                });
                $(".span7").find('input3').eq(i-1).val(sumQ3[i]);
            }

            //.toFixed() method will roundoff the final sum to 2 decimal places
            $("#sum3").val(sum3);
        }
  </script>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <div class="desktable">
    <div class="topnav">
    <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
<div class="topnav1" id="myHeader">
    <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40" style="padding-top:10px ;padding-left:10px">

  <a href="">
  <form method='post' action="">
            <input type="submit" value="Logout" style="cursor:pointer"  name="but_logout" style="background-color:#DCDCDC;">
        </form>
  </a>
  <a href="Report.php">Report</a>
  <a href="History.php">History</a>
      <a  href="calculation.php">Calculation</a>
      
 <a  class="active" href="template.php">Create Template</a>
  <a href="AddEntry.php">Add Entry</a>
  <a href="Dashboard.php">Dashboard</a>
</div>

<div class="card content">
  <div class="header">
         <table style="vertical-align:middle" width="100%">
          <tr>
              <td width="2%" style="vertical-align:middle">
                   <a href="template.php" style="color:black"><i class="fa fa-angle-left" style="font-size:24px"></i></a>
              </td>
              <td width="98%" style="vertical-align:middle;text-align:center">
                    <h3 style="color:black; font-family: 'Montserrat';font-weight:bolder">CREATE NEW TEMPLATE</h3> 
              </td>
          </tr>
      </table>
  </div>
    <form action="" method="post">
    <table width="100%" style="display:block;margin-left:auto;margin-right:auto;padding-left:20px; font-family: 'Montserrat';font-size:14px">
        <tr>
            <td  style="vertical-align:middle">
                  <input class="txt2 input-mini2" name="Name"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center;text-transform: capitalize;" type="text" placeholder="New Template Name" />  

            </td>
         <td  style="vertical-align:middle">
                    <button  name="save"  style="margin-left:10px;padding-left:60px;padding-right:60px"  class="search" >Save</button>

            </td>
    
        </tr>
      
    </table>
 <table width="100%" style="display:block;margin-left:auto;margin-right:auto;padding-left:20px;font-family: 'Montserrat';font-size:14px">
       <tr>
                 <td  width="60%" ><input type="text" id="txtAction" name="extaction"  placeholder="New Action" style="width:100%"/></td>
                <td  width="20%"><input type="text" id="txtpoint" name="extpoint" placeholder="New Point"/></td>
                <td  width="20%">       <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
<input type="text" id="txtprice" style="padding-left:30px;padding: 4px 10px;" placeholder="New Price" name="extprice" /> </div></td>
                <td><input type="button" class="search" style="margin-left:10px;padding-left:50px;padding-right:50px;align:right;border: none;outline:none;margin-right:10px" onclick="Add()" value="Add New Action" /></td>
            </tr>
   </table>
    <table id="tblCustomers" width="100%" class="table table-bordered"   cellpadding="0" cellspacing="0" style="font-family: 'Montserrat';font-size:14px">

  <tr  style="background-color: #B2B2B2;height:50px;text-align:center;color:white;font-size:14px;vertical-align:middle;color:black">
  <th width="60%" style="vertical-align:middle;;text-align:center;">
Action
  </th>
 
  <th width="10%" style="vertical-align:middle;;text-align:center;">

    Point value
  </th>
    <th  style="vertical-align:middle;;text-align:center;" colspan="2">

Price  </th>
   </tr>
  
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
  <textarea name="Refclient"> Referrals of new successful log in as client user</textarea>
  </td>
  <td  style="text-align:center">
  
  <input class="txt2 input-mini2" name="Refclientpoint"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" type="text"/>  
  </td>
    <td  style="text-align:center ;vertical-align: middle;">
             <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="Refclientprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>

  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>
  </td>

  </tr>
  <tr class="green box" style="border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px;">
   <td Style="vertical-align:middle">
         <textarea name="Refbusiness"> Referrals of new successful log in as business user</textarea>
    </td>
    <td  style="text-align:center;">
 <input class="txt2 input-mini2" name="Refbusinesspoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
     <td  style="text-align:center;vertical-align:middle">
      <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="Refbusinessprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </span>
  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <textarea name="RefDelivery">Referrals of new successful log in as Delivery user</textarea>
 
    </td>
    <td  style="text-align:center">
  <input class="txt2 input-mini2" name="RefDeliverypoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
         <td  style="text-align:center;vertical-align:middle">
         <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="RefDeliveryprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </span>

  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
        <textarea name="clientCredit">Client Purchased using Credit card </textarea>
    </td>
    <td  style="text-align:center">
        <input class="txt2 input-mini2" name="clientCreditpoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
      <td  style="text-align:center;vertical-align:middle">
     <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="clientCreditprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
 </div>
  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr >
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="clientCash">Client Purchased using Cash </textarea>
          

    </td>
    <td  style="text-align:center">
            <input class="txt2 input-mini2" name="clientCashpoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
      <td  style="text-align:center;vertical-align:middle">
        <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="clientCashprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>

  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
             <textarea name="client5busi">Client Purchased from 5 different business </textarea>

         
  
    </td>
    <td  style="text-align:center">
          <input class="txt2 input-mini2" name="client5busipoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
      <td  style="text-align:center;vertical-align:middle">
       <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="client5busiprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="client5each">Client Purchased Each 5 times </textarea>
    </td>
    <td  style="text-align:center">
         <input class="txt2 input-mini2" name="client5eachpoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
      <td  style="text-align:center;vertical-align:middle">
   <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
        <input class="txt2 input-mini2" name="client5eachprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>
  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
    <textarea name="busimonth">Client Purchased Each 5 times </textarea>
</td>
    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="busimonthpoint"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
   <td  style="text-align:center">
 <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
<input class="txt2 input-mini2" name="busimonthprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
</div>

  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="busihalfyear">Business Purchased Half Year subscription </textarea>


   
    </td>
    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="busihalfyearpoint"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
          <td  style="text-align:center;vertical-align:middle">
 <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busihalfyearprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="busiyear">Business Purchased Yearly subscription </textarea>

       

 
    </td>
    <td  style="text-align:center">
        <input class="txt2 input-mini" name="busiyearpoint" id="mt22" onkeyup="mbusihalf();"    style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
        <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busiyearprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
           <textarea name="busi5differ">Business Sold to 5 different clients </textarea>
        
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busi5differpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
          <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busi5differprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
    <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
        <textarea name="busi10sale">Business 10 Sales per month </textarea>
       
 
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busi10salepoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
          <td  style="text-align:center;vertical-align:middle">
    <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="busi10saleprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/> 
  </div>

  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
    <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
               <textarea name="busisold10">Business Sold with Credit Card </textarea>

         
 
    </td>
    <td  style="text-align:center">
         <input class="txt2 input-mini"   name="busisold10point"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />


  </td>
          <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busisold10price"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="busipurpush">Business Purchased Push note pack </textarea>

              
  
    </td>
    <td  style="text-align:center">
              <input class="txt2 input-mini"   name="busipurpushpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>

  </td>
          <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busipurpushprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="busipur5push">Business Purchased 5 push note packs </textarea>
        
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busipur5pushpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
         <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busipur5pushprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/> 
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  
  
   <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <textarea name="busAMBA">Business Subscription - AMBASSADOR REFERRAL Purchased Monthly subscription </textarea>
      
</td>
    <td  style="text-align:center">
       <input  name="busAMBApoint" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
      <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input class="txt2 input-mini2" name="busAMBAprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <textarea name="buspusha">Business Purchased Push note pack A-5 notes </textarea>
      
     

    <td  style="text-align:center">
       <input name="buspushapoint" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
         <td  style="text-align:center;vertical-align:middle">
     <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input  name="buspushaprice" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <textarea name="buspushb">Business Purchased Push note pack B-10 notes </textarea>
     
    <td  style="text-align:center">
       <input  name="buspushbpoint" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
         <td  style="text-align:center;vertical-align:middle">
      <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>
  <input name="buspushbprice"style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
   <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
              <textarea name="buspushc">Business Purchased Push note pack C-20 notes</textarea>
      
    <td  style="text-align:center">
       <input name="buspushcpoint" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
         <td  style="text-align:center;vertical-align:middle">
     <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input  name="buspushcprice" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>

  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>

    
  </td>
  </tr>
  
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <textarea name="deliveryitem">Delivery Items delivered complete notes</textarea>
           
  
    </td>
  
          <td  style="text-align:center;vertical-align:middle">

       <input class="txt2 input-mini2" name="deliveryitempoint"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" type="text"/>  
 
  </td>
        <td  style="text-align:center;vertical-align:middle">
    <div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="deliveryitemprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>


  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <textarea name="delivery5complete">Delivery completed 5 deliveries</textarea>
        
  
    </td>
   
  <td  style="text-align:center">

         <input class="txt2 input-mini"   name="delivery5completepoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
         <td  style="text-align:center;vertical-align:middle">
<div id="floatContainer" class="float-container" style="text-align:center">
        <label for="floatField" style="margin-top: 10px; margin-left: 10px;">$</label>

  <input class="txt2 input-mini2" name="delivery5completeprice"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;padding: 4px 10px;" type="text"/>  
  </div>

  </td>
 <td  style="text-align:center;vertical-align:middle">
  <button class="btnDelete" onclick="Remove(this)">Remove</button>


    
  </td>
  </tr>
        
  </table>
  </table>
  </from>
   </div> 
      </div> 
<script>
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("." + inputValue).toggle();
    });
});
</script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>


<!-- Mobile View -->
<style>
@media only screen and (max-width: 600px) {
  .mbtable{
display:block;
}
.desktable{
  display:none;
}
  div.mcard {
  width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
.topnav {
  overflow: hidden;
  background-color: FBB040;
  height:50px;
}

body { margin: 0 auto; }

header {
	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;
}
footer {
	height: 50px;
	background-color: #494949;
	color: #fff;
	display: flex;
	align-items: center;
	justify-content: center;
	bottom: 0;
	position: fixed;
	width: 100%;
}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
.topnav1 {
  overflow: hidden;
 background-color:B2B2B2;
  height:50px;
}
.topnav1 a {
  color: #444;
  /* text-align: center; */
  padding: 14px 16px;
  text-decoration: none;
  font-size: 10px;
}
.topnav1 a.active {
  color: #d27c85;
  font-weight:bold;
}
div.card1 {
  width: 95%;
  text-align: center;
  margin-left:10px;
  margin-top:20px;

}
@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
body { margin: 0 auto; }

header {
	height: 60px;
 background-color:B2B2B2;

	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;

}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
  .topnav a:not(:first-child), .dropdown .dropbtn {
    display: none;
  }
  .topnav1 a.icon {
    float: right;
    display: block;
  }
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

}

@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

  .topnav1.responsive {position: relative;}
  .topnav1.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav1.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav1.responsive .dropdown {float: none;}
  .topnav1.responsive .dropdown-content {position: relative;}
  .topnav1.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}
}
input[type=checkbox]{
  margin:5px;
  width: 1.3em;
    height: 1.3em;
    background-color: white;
    border-radius: 50%;
    vertical-align: middle;
    border: 1px solid #ddd;
        outline: none;
    cursor: pointer;
}
.row2{ 
    border-bottom: 1px solid #E8E8E8;
    height:60px;
    font-size:14px;
}
 </style>


 <div class="mbtable">
  <div class="topnav">
     <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
 <header>
 
		<div class="top">
    <table>
    <tr>
    <td>
        <a href="#" style=" float: right;" class="menu_icon"><i style=" float: left;"  class="material-icons">dehaze</i></a>

    </td>
    <td style="padding-right:15px">
      
 <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40"  style="margin-left:60%">



    </td>
    </tr>
    </table>

		</div>
	</header>
	<nav class="menu">
  <a  class="item_menu" href="Dashboard.php">Dashboard</a>
  <a  class="item_menu" href="AddEntry.php">Add Entry</a>
   <a  class="active" href="template.php">Create Template</a>

  <a  class="item_menu" href="History.php">History</a>
  <a  class="item_menu" href="Report.php">Report</a>
  <a href="#"  class="item_menu">
  <form method='post' action="">
            <input type="submit" value="Logout" name="but_logout">
        </form>
  </a>
	</nav>
<div class="mcard">
  <div class="header">
   
                    <h3 style="color:white">CREATE NEW TEMPLATE</h3> 
           
  </div>
  <form action="" method="post">
    <table width="100%" style="display:block;margin-left:auto;margin-right:auto;padding-left:30px;">
        <tr>
            <td style="vertical-align:middle" colspan="2">
                  <input class="txt2 input-mini2" name="Name" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" type="text" placeholder="New Template Name"/>  

            </td>
            </tr>
        <tr height="60px">
         <td  style="vertical-align:middle">
                    <button  name="save" class="btn btn-primary" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" >Save</button>

            </td>
        <td   style="vertical-align:middle;text-align:right;">
                  
                   
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<button type="button" name="add"  id="add_row"  style="margin-left:10px;padding-left:30px;padding-right:30px;align:right;border: none;outline:none;"  class="search">Add New Action</button>

            </td>
            </tr>
      
    </table>
 <table width="80%"   class="table table-bordered"  style="margin-left:auto;margin-right:auto">
  <tr  style="background-color: black;height:50px;text-align:center;color:white;font-size:14px;vertical-align:middle;">
  <th style="vertical-align:middle;;text-align:center;">
Action
  </th>
 
  <th style="vertical-align:middle;;text-align:center;">

    Point value
  </th>
   </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">

  <input class="txt2 input-mini2" name="Refclient"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" type="text" value="  Referrals of new successful log in as client user"/>  
  </td>
  <td  style="text-align:center">
  
  <input class="txt2 input-mini2" name="Refclientpoint"   id="textmone"  onkeyup="msumclogin();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" type="text"/>  
  </td>
 
  </tr>
  <tr class="green box" style="border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px;">
   <td Style="vertical-align:middle">
 <input class="txt2 input-mini2" name="Refbusiness"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Referrals of new successful log in as business user "/>  
 
    </td>
    <td  style="text-align:center;">
 <input class="txt2 input-mini2" name="Refbusinesspoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
  </tr>
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
  <input class="txt2 input-mini2" name="RefDelivery"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Referrals of new successful log in as Delivery user "/>  
  
    </td>
    <td  style="text-align:center">
  <input class="txt2 input-mini2" name="RefDeliverypoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
        <input class="txt2 input-mini2" name="clientCredit"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Client Purchased using Credit card  "/>  
  
    </td>
    <td  style="text-align:center">
        <input class="txt2 input-mini2" name="clientCreditpoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
 
  </tr >
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
            <input class="txt2 input-mini2" name="clientCash"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="  Client Purchased using Cash "/>  

    </td>
    <td  style="text-align:center">
            <input class="txt2 input-mini2" name="clientCashpoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
          <input class="txt2 input-mini2" name="client5busi"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Client Purchased from 5 different business "/>  
  
    </td>
    <td  style="text-align:center">
          <input class="txt2 input-mini2" name="client5busipoint"  id="mt4" onkeyup="msumdlogin();"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />  

  </td>
  </tr>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
         <input  name="client5each" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Client Purchased Each 5 times "/>  
  
    </td>
    <td  style="text-align:center">
         <input  name="client5eachpoint" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
   <td  style="text-align:center">
         <input  name="client5eachprice" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>
  </tr>
   <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <input name="busAMBA"   style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business Subscription - AMBASSADOR REFERRAL Purchased Monthly subscription"/>  

    <td  style="text-align:center">
       <input name="busAMBApoint"style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
   <td  style="text-align:center">
       <input name="busAMBAprice" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <input class="txt2 input-mini"   name="busimonth"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="
  Business Purchased Monthly subscription "/>  

    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="busimonthpoint"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
 
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
         <input class="txt2 input-mini"   name="busihalfyear"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="
  Business Purchased Half Year subscription "/>  

   
    </td>
    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="busihalfyearpoint"  id="mt16" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
        <input class="txt2 input-mini" name="busiyear" id="mt22" onkeyup="mbusihalf();"    style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Business Purchased Yearly subscription "/>  

 
    </td>
    <td  style="text-align:center">
        <input class="txt2 input-mini" name="busiyearpoint" id="mt22" onkeyup="mbusihalf();"    style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  

  </td>

  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
         <input class="txt2 input-mini"   name="busi5differ"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="  Business Sold to 5 different clients" />
 
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busi5differpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
  </tr>
    <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
         <input class="txt2 input-mini"   name="busi10sale"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business 10 Sales per month" />
 
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busi10salepoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <input class="txt2 input-mini"   name="buspusha"  id="mt31" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business Purchased Push note pack A-5 notes"/>  

    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="buspushapoint"  id="mt32" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
 
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <input class="txt2 input-mini"   name="buspushb"  id="mt33" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business Purchased Push note pack B-10 notes"/>  

    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="buspushbpoint"  id="mt34" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
 
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
       <input class="txt2 input-mini"   name="buspushc"  id="mt35" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business Purchased Push note pack C-20 notes"/>  

    <td  style="text-align:center">
       <input class="txt2 input-mini"   name="buspushcpoint"  id="mt36" onkeyup="mclienteach();" style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>  
  </td>
 
  </tr>
  
  
  
    <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
   <td Style="vertical-align:middle">
         <input class="txt2 input-mini"   name="busisold10"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value="Business Sold with Credit Card" />
 
    </td>
    <td  style="text-align:center">
         <input class="txt2 input-mini"   name="busisold10point"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />


  </td>
  </tr>
  
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
              <input class="txt2 input-mini"   name="busipurpush"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Business Purchased Push note pack " />
  
    </td>
    <td  style="text-align:center">
              <input class="txt2 input-mini"   name="busipurpushpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text"/>

  </td>
  </tr>
  <tr class="green box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
          <input class="txt2 input-mini"   name="busipur5push"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Business Purchased 5 push note packs" />
 
    </td>
    <td  style="text-align:center">

         <input class="txt2 input-mini"   name="busipur5pushpoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
 
  </tr>
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
            <input class="txt2 input-mini"   name="deliveryitem"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Delivery Items delivered complete" />
  
    </td>
  
  <td>
         <input class="txt2 input-mini"   name="deliveryitempoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>

  </tr>
  <tr class="blue box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:14px">
  <td Style="vertical-align:middle">
         <input class="txt2 input-mini"   name="delivery5complete"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" value=" Delivery completed 5 deliveries" />
  
    </td>
   
  <td  style="text-align:center">

         <input class="txt2 input-mini"   name="delivery5completepoint"  id="mt28" onkeyup="mbusi5();"  style="width:100%;margin-left:auto;margin-right:auto;text-align:center" type="text" />

  </td>
 
  </tr>
           <tr id='addr1'></tr>
  </table>
  </table>
  </from>
   </div> 
      </div> 

 </div>
       <script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
<script>
function sumclogin() {
           var txtFirstNumberValue = document.getElementById('textone').value;
           var txtSecondNumberValue = document.getElementById('texttwo').value;
           var result = txtFirstNumberValue * txtSecondNumberValue;
           if (!isNaN(result)) {
               document.getElementById('textthree').value = result;
           }
       }
       function sumblogin() {
           var txtFirstNumberValue1 = document.getElementById('t1').value;
           var txtSecondNumberValue2 = document.getElementById('t2').value;
           var result = txtFirstNumberValue1 * txtSecondNumberValue2;
           if (!isNaN(result)) {
               document.getElementById('t3').value = result;
           }
       }
       function sumdlogin() {
           var txtFirstNumberValue3 = document.getElementById('t4').value;
           var txtSecondNumberValue4 = document.getElementById('t5').value;
           var result = txtFirstNumberValue3 * txtSecondNumberValue4;
           if (!isNaN(result)) {
               document.getElementById('t6').value = result;
           }
       }
       function clientcredit() {
           var txtFirstNumberValue5 = document.getElementById('t7').value;
           var txtSecondNumberValue6 = document.getElementById('t8').value;
           var result = txtFirstNumberValue5 * txtSecondNumberValue6;
           if (!isNaN(result)) {
               document.getElementById('t9').value = result;
           }
       }
       function clientcash() {
           var txtFirstNumberValue7 = document.getElementById('t10').value;
           var txtSecondNumberValue8 = document.getElementById('t11').value;
           var result = txtFirstNumberValue7 * txtSecondNumberValue8;
           if (!isNaN(result)) {
               document.getElementById('t12').value = result;
           }
       }
       function clientbus() {
           var txtFirstNumberValue9 = document.getElementById('t13').value;
           var txtSecondNumberValue10 = document.getElementById('t14').value;
           var result = txtFirstNumberValue9 * txtSecondNumberValue10;
           if (!isNaN(result)) {
               document.getElementById('t15').value = result;
           }
       }
       function clienteach() {
           var txtFirstNumberValue11 = document.getElementById('t16').value;
           var txtSecondNumberValue12 = document.getElementById('t17').value;
           var result = txtFirstNumberValue11 * txtSecondNumberValue12;
           if (!isNaN(result)) {
               document.getElementById('t18').value = result;
           }
       }
       function busimonth() {
           var txtFirstNumberValue13 = document.getElementById('t19').value;
           var txtSecondNumberValue14 = document.getElementById('t20').value;
           var result = txtFirstNumberValue13 * txtSecondNumberValue14;
           if (!isNaN(result)) {
               document.getElementById('t21').value = result;
           }
       }
       

       function busihalf () {
           var txtFirstNumberValue15 = document.getElementById('t22').value;
           var txtSecondNumberValue16 = document.getElementById('t23').value;
           var result = txtFirstNumberValue15 * txtSecondNumberValue16;
           if (!isNaN(result)) {
               document.getElementById('t24').value = result;
           }
       }
       

       function busiyear () {
           var txtFirstNumberValue17 = document.getElementById('t25').value;
           var txtSecondNumberValue18 = document.getElementById('t26').value;
           var result = txtFirstNumberValue17 * txtSecondNumberValue18;
           if (!isNaN(result)) {
               document.getElementById('t27').value = result;
           }
       }
       

       function busi5() {
           var txtFirstNumberValue18= document.getElementById('t28').value;
           var txtSecondNumberValue19 = document.getElementById('t29').value;
           var result = txtFirstNumberValue18 * txtSecondNumberValue19;
           if (!isNaN(result)) {
               document.getElementById('t30').value = result;
           }
       }
       

       function busisale() {
           var txtFirstNumberValue21 = document.getElementById('t31').value;
           var txtSecondNumberValue22 = document.getElementById('t32').value;
           var result = txtFirstNumberValue21 * txtSecondNumberValue22;
           if (!isNaN(result)) {
               document.getElementById('t33').value = result;
           }
       }
       

       function busisoldcre() {
           var txtFirstNumberValue22 = document.getElementById('t34').value;
           var txtSecondNumberValue23 = document.getElementById('t35').value;
           var result = txtFirstNumberValue22 * txtSecondNumberValue23;
           if (!isNaN(result)) {
               document.getElementById('t36').value = result;
           }
       }
       

       function busipush() {
           var txtFirstNumberValue23 = document.getElementById('t37').value;
           var txtSecondNumberValue24 = document.getElementById('t38').value;
           var result = txtFirstNumberValue23 * txtSecondNumberValue24;
           if (!isNaN(result)) {
               document.getElementById('t39').value = result;
           }
       }
       

       function busi5push() {
           var txtFirstNumberValue25 = document.getElementById('t40').value;
           var txtSecondNumberValue26 = document.getElementById('t41').value;
           var result = txtFirstNumberValue25 * txtSecondNumberValue26;
           if (!isNaN(result)) {
               document.getElementById('t42').value = result;
           }
       }
       

       function divitem() {
           var txtFirstNumberValue27 = document.getElementById('t43').value;
           var txtSecondNumberValue28 = document.getElementById('t44').value;
           var result = txtFirstNumberValue27 * txtSecondNumberValue28;
           if (!isNaN(result)) {
               document.getElementById('t45').value = result;
           }
       }
       

       function div5item() {
           var txtFirstNumberValue28 = document.getElementById('t46').value;
           var txtSecondNumberValue29 = document.getElementById('t47').value;
           var result = txtFirstNumberValue28 * txtSecondNumberValue29;
           if (!isNaN(result)) {
               document.getElementById('t48').value = result;
           }
       }
       function totalpoint() {
           var txtFirstNumberValue1 = document.getElementById('t1').value;
           var txtFirstNumberValue28 = document.getElementById('t46').value;
           var txtFirstNumberValue27 = document.getElementById('t43').value;

           var result = txtFirstNumberValue1 + txtFirstNumberValue28 + txtFirstNumberValue27;
           if (!isNaN(result)) {
               document.getElementById('t3').value = result;
           }
       }
       
</script>

<script>
  $(document).ready(function(){
    $('.calc').change(function(){
        var total = 0;
        $('.calc').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total').html(total);
    });
})(jQuery);
</script>
<script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
  
  <script>
function msumclogin() {
           var txtmFirstNumberValue = document.getElementById('textmone').value;
           var txtmSecondNumberValue = document.getElementById('textmtwo').value;
           var result = txtmFirstNumberValue * txtmSecondNumberValue;
           if (!isNaN(result)) {
               document.getElementById('textmthree').value = result;
           }
       }
       function msumblogin() {
           var txtFirstNumbermValue1 = document.getElementById('mt1').value;
           var txtSecondNumbermValue2 = document.getElementById('mt2').value;
           var result = txtFirstNumbermValue1 * txtSecondNumbermValue2;
           if (!isNaN(result)) {
               document.getElementById('mt3').value = result;
           }
       }
       function msumdlogin() {
           var txtFirstNumbermValue3 = document.getElementById('mt4').value;
           var txtSecondNumbermValue4 = document.getElementById('mt5').value;
           var result = txtFirstNumbermValue3 * txtSecondNumbermValue4;
           if (!isNaN(result)) {
               document.getElementById('mt6').value = result;
           }
       }
       function mclientcredit() {
           var txtFirstNumbermValue5 = document.getElementById('mt7').value;
           var txtSecondNumbermValue6 = document.getElementById('mt8').value;
           var result = txtFirstNumbermValue5 * txtSecondNumbermValue6;
           if (!isNaN(result)) {
               document.getElementById('mt9').value = result;
           }
       }
       function mclientcash() {
           var txtFirstNumbermValue7 = document.getElementById('mt10').value;
           var txtSecondNumbermValue8 = document.getElementById('mt11').value;
           var result = txtFirstNumbermValue7 * txtSecondNumbermValue8;
           if (!isNaN(result)) {
               document.getElementById('mt12').value = result;
           }
       }
       function mclientbus() {
           var txtFirstNumbermValue9 = document.getElementById('mt13').value;
           var txtSecondNumbermValue10 = document.getElementById('mt14').value;
           var result = txtFirstNumbermValue9 * txtSecondNumbermValue10;
           if (!isNaN(result)) {
               document.getElementById('mt15').value = result;
           }
       }
       function mclienteach() {
           var txtFirstNumbermValue11 = document.getElementById('mt16').value;
           var txtSecondNumbermValue12 = document.getElementById('mt17').value;
           var result = txtFirstNumbermValue11 * txtSecondNumbermValue12;
           if (!isNaN(result)) {
               document.getElementById('mt18').value = result;
           }
       }
       function mbusimonth() {
           var txtFirstNumbermValue13 = document.getElementById('mt19').value;
           var txtSecondNumbermValue14 = document.getElementById('mt20').value;
           var result = txtFirstNumbermValue13 * txtSecondNumbermValue14;
           if (!isNaN(result)) {
               document.getElementById('mt21').value = result;
           }
       }
       

       function mbusihalf () {
           var txtFirstNumbermValue15 = document.getElementById('mt22').value;
           var txtSecondNumbermValue16 = document.getElementById('mt23').value;
           var result = txtFirstNumbermValue15 * txtSecondNumbermValue16;
           if (!isNaN(result)) {
               document.getElementById('mt24').value = result;
           }
       }
       

       function mbusiyear () {
           var txtFirstNumbermValue17 = document.getElementById('mt25').value;
           var txtSecondNumbermValue18 = document.getElementById('mt26').value;
           var result = txtFirstNumbermValue17 * txtSecondNumbermValue18;
           if (!isNaN(result)) {
               document.getElementById('mt27').value = result;
           }
       }
       

       function mbusi5() {
           var txtFirstNumbermValue18= document.getElementById('mt28').value;
           var txtSecondNumbermValue19 = document.getElementById('mt29').value;
           var result = txtFirstNumbermValue18 * txtSecondNumbermValue19;
           if (!isNaN(result)) {
               document.getElementById('mt30').value = result;
           }
       }
       

       function mbusisale() {
           var txtFirstNumbermValue21 = document.getElementById('mt31').value;
                    var txtSecondNumbermValue22 = document.getElementById('mt32').value;
           var result = txtFirstNumbermValue21 * txtSecondNumbermValue22;
           if (!isNaN(result)) {
               document.getElementById('mt33').value = result;
           }
       }
       

       function mbusisoldcre() {
           var txtFirstNumbermValue22 = document.getElementById('mt34').value;
           var txtSecondNumbermValue23 = document.getElementById('mt35').value;
           var result = txtFirstNumbermValue22 * txtSecondNumbermValue23;
           if (!isNaN(result)) {
               document.getElementById('mt36').value = result;
           }
       }
       

       function mbusipush() {
           var txtFirstNumbermValue23 = document.getElementById('mt37').value;
           var txtSecondNumbermValue24 = document.getElementById('mt38').value;
           var result = txtFirstNumbermValue23 * txtSecondNumbermValue24;
           if (!isNaN(result)) {
               document.getElementById('mt39').value = result;
           }
       }
       

       function mbusi5push() {
           var txtFirstNumbermValue25 = document.getElementById('mt40').value;
           var txtSecondNumbermValue26 = document.getElementById('mt41').value;
           var result = txtFirstNumbermValue25 * txtSecondNumbermValue26;
           if (!isNaN(result)) {
               document.getElementById('mt42').value = result;
           }
       }
       

       function mdivitem() {
           var txtFirstNumbermValue27 = document.getElementById('mt43').value;
           var txtSecondNumbermValue28 = document.getElementById('mt44').value;
           var result = txtFirstNumbermValue27 * txtSecondNumbermValue28;
           if (!isNaN(result)) {
               document.getElementById('mt45').value = result;
           }
       }
       

       function mdiv5item() {
           var txtFirstNumbermValue28 = document.getElementById('mt46').value;
           var txtSecondNumbermValue29 = document.getElementById('mt47').value;
           var result = txtFirstNumbermValue28 * txtSecondNumbermValue29;
           if (!isNaN(result)) {
               document.getElementById('mt48').value = result;
           }
       }
       function mtotalpoint() {
           var txtFirstNumbermValue1 = document.getElementById('mt1').value;
           var txtFirstNumbermValue28 = document.getElementById('mt46').value;
           var txtFirstNumberValue27 = document.getElementById('t43').value;

           var result = txtFirstNumbermValue1 + txtFirstNumbermValue28 + txtFirstNumbermValue27;
           if (!isNaN(result)) {
               document.getElementById('mt3').value = result;
           }
       }
       
</script>
<!-- <input type="text" class="calc" value="">
<input type="text" class="calc" value="">
<input type="text" class="calc" value="">
<input type="text" class="calc" value="">
<input type="text" class="calc" value="">

<span id="total"></span> -->
<script>
  $(document).ready(function(){
    $('.calc').change(function(){
        var total = 0;
        $('.calc').each(function(){
            if($(this).val() != '')
            {
                total += parseInt($(this).val());
            }
        });
        $('#total').html(total);
    });
})(jQuery);
</script>
<script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
  <script>
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("." + inputValue).toggle();
    });
});
</script>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script>
 id="tbUser"</script>
<script>
$("#tbUser").on('click', '.btnDelete', function () {
    $(this).closest('tr').remove();
});
</script>
<script>
$(document).ready(function() {
  var i = 1;
  $("#add_row").click(function() {
  $('tr').find('input').prop('disabled',false)

    $('#addr' + i).html("<td>  <textarea  name='uid" + i + "'> Referrals of new successful log in as client user</textarea></td><td><input type='text' name='uname" + i + "'  style='width:50%;margin-left:auto;margin-right:auto;text-align:center;display:block'/></td><td><input type='text' name='uname" + i + "'style='width:50%;display:block;margin-left:auto;margin-right:auto;text-align:center'/><button  class='btnDelete'><i class='fa fa-close'></i></button></td>");

    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
    i++;
  });
});
</script>
<script>
$("#tbUser").on('click', '.btnDelete', function () {
    $(this).closest('tr').remove();
});
</script>
<script>
    $('#send').click(function(){ 
$(':input[type="text"]').filter(function(e){
    if (this.value.length===0){
      return true;
    }  
}).remove();
    });
</script>
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
</head>
<body>

</body>
</html>
</body>
</html>
