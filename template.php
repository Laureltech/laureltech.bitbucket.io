<?php
include "Connection.php";

// Check user login or not
if(!isset($_SESSION['uname'])){
    header('Location: Login.php');
}

// logout
if(isset($_POST['but_logout'])){
    session_destroy();
    header('Location: Login.php');
}


?>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> abierTTo Admin</title> 
    <link href="https://www.smarteyeapps.com/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.smarteyeapps.com/assets/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<style type="text/css">
.mbtable{
display:none;
}
.desktable{
  display:block;
}
input[type=text] {
  width: 50%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
input[type=text]:focus {
    outline: none;
}
.container1 {
  display: block;
  position: relative;
  padding-left: 22px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;

  user-select: none;
}

/* Hide the browser's default checkbox */
.container1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 18px;
  width: 18px;
  border: solid 1px  #2196F3;
 border-radius: 50%;
}


/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {usi5differ,busi10sale,busisold10,busipurpush,busipur5push,deliveryitem,
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
  left: 4px;
  top: 2px;
  width: 7px;
  height: 11px;
  border: .5px solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
div.card {
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
}
div.card1 {
  width: 97%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;
}
div.header {
  background-color: #B2B2B2;
  color: black;
  padding: 8px;
  font-size: 10px;
}

div.container {
  padding: 0px;
  font-size: 2em;
    box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
.topnav {
  overflow: hidden;
  background-color: #FBB040;
  height:50px;
}
.topnav1 {
  overflow: hidden;
   background-color:#DCDCDC;

  height:50px;
}
input[type="submit"] {
border: none;
outline:none;
background-color:transparent;
}
.topnav1 a {
    float: right;
  color: #444;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 14px;

  font-weight:bold;
}
.topnav1 a.active {
  color: #E31E36;
  font-weight:bold;
}
#myDIV {
  width: 80%;
  padding-top:10px;
  margin-left:auto;
  margin-right:auto;
  margin-top: 20px;
      box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
h3,h4,h6{
    margin-top: 10px;
}
.search{
  background-color: #5CC4BA;
 border-radius: 2rem; 
  border: none;
  color: white;
  padding-top:10px;
  padding-bottom:10px;
  padding-left:40px;
  padding-right:40px;
  text-decoration: none;
  margin: 4px 2px;
  font-weight:bold;
  cursor: pointer;
}
.content {
  position: inherit;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
   padding-top: 10px;
}
</style>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
</head>
<body>
  <div class="desktable">
    <div class="topnav">
     <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
<div class="topnav1" id="myHeader">
  <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40" style="padding-top:10px ;padding-left:10px">
  <a href="#news"><form method='post' action=""><input type="submit" value="Logout" name="but_logout" style="cursor:pointer" ></form></a>
  <a href="Report.php">Report</a>
  <a  href="History.php">History</a>
     <a  href="calculation.php">Calculation</a>
      <a class="active" href="template.php">Create Template</a>

    <!--<a href="StockCalculation.php">Stock Calculation</a>-->
  <a href="AddEntry.php">Add Entry</a>
  <a  href="Dashboard.php">Dashboard</a>
</div>


  </div>
<div class="card content">
  <div class="header">
   <h3 style="font-weight:bold">ACTION TEMPLATES</h3>
  </div>
  <form action="create_temp.php" class="inline" style="text-align:left">
<button onclick="myFunction()" class="search" style="width:30%;margin-top:2%;margin-bottom:2%;margin-left:5%;border: none;outline:none;"  >Create Template</button>
</form>

      <form action="" method="post">
<table width="100%">

  <tbody id="geeks" >
  <?php
        $sql="select * from template";
        $result1=mysqli_query($con,$sql);
		foreach($result1 as $row)
		{

		?>
  <tr  class="red box" style=" border-bottom: 1px solid #E8E8E8;height:60px;font-size:16px;padding-left:60px;font-size:14px">
  <td  width="80%"  style="vertical-align:middle;padding-left:60px" >
  <?php echo $row['Name']; ?>
  </td>
  <td  width="10%" style="text-align:center">
        <from>
        <a href="Edit_template.php?ID=<?php echo $row['ID']; ?>" class="search"  style="padding-left:50px;padding-right:50px;margin-right:10px;text-decoration:none;color:white">Edit</a>
             </from>

    </td>
  <td  width="10%" style="text-align:center">
      <from>
               <a  href="Delete_temp.php?Name=<?php echo $row['Name']; ?>" class="search" style="padding-left:40px;padding-right:40px;margin-right:10px;text-decoration:none;color:white"  onclick="return confirm('Are you sure you want to delete this Template')" >Delete</a>

      </from>
                   <!--<a href="template.php" style="color:#444">Delete</a>-->
  </td>
  </tr>
 
  <?php
		}
		?>	
        </tbody>
  </table>
</form>
  </div>
<!-- Button trigger modal -->




</div>


<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>


<!-- Mobile View -->
<style>
@media only screen and (max-width: 600px) {
  .mbtable{
display:block;
}
.desktable{
  display:none;
}
input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
  width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
.topnav {
  overflow: hidden;
  background-color:FBB040;
  height:50px;
}

body { margin: 0 auto; }

header {
	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;
}
footer {
	height: 50px;
	background-color: #494949;
	color: #fff;
	display: flex;
	align-items: center;
	justify-content: center;
	bottom: 0;
	position: fixed;
	width: 100%;
}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
.topnav1 {
  overflow: hidden;
  background-color:rgba(242,242,242,255);
  height:50px;
}
.topnav1 a {
  color: #444;
  /* text-align: center; */
  padding: 14px 16px;
  text-decoration: none;
  font-size: 10px;
}
.topnav1 a.active {
  color: #d27c85;
  font-weight:bold;
}
div.card1 {
  width: 95%;
  text-align: center;
  margin-left:10px;
  margin-top:20px;

}
@media screen and (max-width: 600px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
body { margin: 0 auto; }

header {
	height: 60px;
  background-color:rgba(242,242,242,255);

	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;

}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
  .topnav a:not(:first-child), .dropdown .dropbtn {
   
  }
  .topnav1 a.icon {
    float: right;
    display: block;
  }
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

}

@media screen and (max-width: 600px) {
    input[type=text] {
  width: 90%;
    box-sizing: border-box;
    margin-left: 1px;
    margin-right: auto;
    border: 1px solid #D3D3D3;
    display: block;
    margin-top: 20px;
    font-size:15px;
    padding: 12px 20px;
    margin: 8px 0;
    margin-right:20px;
    margin-bottom:20px;
     margin-top:20px;
    float: right;
}
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

  .topnav1.responsive {position: relative;}
  .topnav1.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav1.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav1.responsive .dropdown {float: none;}
  .topnav1.responsive .dropdown-content {position: relative;}
  .topnav1.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}
}
 </style>


 <div class="mbtable">
 <div class="topnav">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
<header>
 
		<div class="top">
    <table>
    <tr>
    <td>
        <a href="#" style=" float: right;" class="menu_icon"><i style=" float: left;"  class="material-icons">dehaze</i></a>

    </td>
    <td style="padding-right:15px">
        <img src="./Images/abier.png" alt="Girl in a jacket" width="200" height="50"  style="margin-left:50%">


    </td>
    </tr>
    </table>

		</div>
	</header>

	<nav class="menu">
  <a  class="item_menu" href="Dashboard.php">Dashboard</a>
  <a  class="item_menu" href="AddEntry.php">Add Entry</a>
  <a  class="item_menu" href="History.php">History</a>
  <a  class="item_menu" class="active" href="Report.php">Report</a>
  <a href="#"  class="item_menu">
  <form method='post' action="">
            <input type="submit" value="Logout" name="but_logout">
        </form>
  </a>
	</nav>
	<main>
    <div class="mcard">
    <form action="" method="post">
        
</form>
</div>

	</main>
</div>
 </body>

      <script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
  <script>
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("." + inputValue).toggle();
    });
});
</script>

<script>
            $(document).ready(function() {
                $("#gfg").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#geeks tr").filter(function() {
                        $(this).toggle($(this).text()
                        .toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $("#mgfg").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#mgeeks tr").filter(function() {
                        $(this).toggle($(this).text()
                        .toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
        <script>
function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>

<script>
$(document).ready(function() {
  var i = 1;
  $("#add_row").click(function() {
  $('tr').find('input').prop('disabled',false)

    $('#addr' + i).html("<td><input type='text' name='uid" + i + "'  style='width:100%;margin-left:auto;margin-right:auto;text-align:center'/></td><td><input type='text' name='uname" + i + "'  style='width:100%;margin-left:auto;margin-right:auto;text-align:center'/></td>");

    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
    i++;
  });
});
</script>
</html>