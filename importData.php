
<?php
// Load the database configuration file
include 'Connection.php';
// print_r($_POST);
if(isset($_POST['importSubmit'])){

// Allowed mime types
$csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

// Validate whether selected file is a CSV file
if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){

// If the file is uploaded
if(is_uploaded_file($_FILES['file']['tmp_name'])){

// Open uploaded CSV file with read-only mode
$csvFile = fopen($_FILES['file']['tmp_name'], 'r');

// Skip the first line
fgetcsv($csvFile);

// Parse data from CSV file line by line
while(($line = fgetcsv($csvFile)) !== FALSE){
// Get row data
$userName = $line[2];
$userType = $line[3];
$clientReferralsqty = $line[4];
$clientReferralspoints = $line[5];
$clientpurchasecreditqty = $line[6];
$clientpurchasecreditpoints = $line[7];
$clientpurchasecashqty = $line[8];
$clientpurchasecashpoints = $line[9];
$clientpurchasedifferqty = $line[10];
$clientpurchasedifferpoints = $line[11];
$clientpurchasedeachqty = $line[12];
$businessReferralsqty = $line[13];
$businessReferralspoints = $line[14];
$businessPurchasedMonthlyqty = $line[15];
$businessPurchasedMonthlypoints = $line[16];
$businessPurchasedHalfqty = $line[17];
$businessPurchasedHalfpoints = $line[18];
$businessPurchasedyearqty = $line[19];
$businessPurchasedyearpoints = $line[20];
$businessSoldqty = $line[21];
$businessSoldpoints = $line[22];
$businessSaleMonthqty = $line[23];
$businessSaleMonthpoints = $line[24];
$businessSoldCreditqty = $line[25];
$businessSoldCreditpoints = $line[26];
$businessPurchasedPushqty = $line[27];
$businessPurchasedPushpoints = $line[28];
$businessPurchasedPushNoteqty = $line[29];
$businessPurchasedPushNotepoints = $line[30];
$deliveryReferralsqty = $line[31];
$deliveryReferralspoints = $line[32];
$deliveryItemqty = $line[33];
$deliveryItempoints = $line[34];
$deliverycompletedqty = $line[35];
$deliverycompletedpoints = $line[36];
$Totalqty = $line[37];
$Totalpoint = $line[38];
// Check whether member already exists in the database with the same email
$prevQuery = "SELECT userName FROM points WHERE userName = '".$line[1]."'";
$prevResult = mysqli_query($con,$prevQuery);

if(mysqli_num_rows($prevResult) > 0){
// Update member data in the database
mysqli_query($con,"UPDATE points SET 
userName = '".$userName."',
userType = '".$userType."',
clientReferralsqty = '".$clientReferralsqty."',
clientReferralspoints = '".$clientReferralspoints."',
clientpurchasecreditqty = '".$clientpurchasecreditqty."',
clientpurchasecreditpoints = '".$clientpurchasecreditpoints."',
clientpurchasecashqty = '".$clientpurchasecashqty."',
clientpurchasecashpoints = '".$clientpurchasecashpoints."',
clientpurchasedifferqty = '".$clientpurchasedifferqty."',
clientpurchasedifferpoints = '".$clientpurchasedifferpoints."',
clientpurchasedeachqty = '".$clientpurchasedeachqty."',
clientpurchasedeachpoints = '".$clientpurchasedeachpoints."',
businessReferralsqty = '".$businessReferralsqty."',
businessReferralspoints = '".$businessReferralspoints."',
businessPurchasedMonthlyqty = '".$businessPurchasedMonthlyqty."',
businessPurchasedMonthlypoints = '".$businessPurchasedMonthlypoints."',
businessPurchasedHalfqty = '".$businessPurchasedHalfqty."',
businessPurchasedHalfpoints = '".$businessPurchasedHalfpoints."',
businessPurchasedyearqty = '".$businessPurchasedyearqty."',
businessPurchasedyearpoints = '".$businessPurchasedyearpoints."',
businessSoldqty = '".$businessSoldqty."',
businessSoldpoints = '".$businessSoldpoints."',
businessSaleMonthqty = '".$businessSaleMonthqty."',
businessSaleMonthpoints = '".$businessSaleMonthpoints."',
businessSoldCreditqty = '".$businessSoldCreditqty."',
businessSoldCreditpoints = '".$businessSoldCreditpoints."',
businessPurchasedPushqty = '".$businessPurchasedPushqty."',
businessPurchasedPushpoints = '".$businessPurchasedPushpoints."',
businessPurchasedPushNoteqty = '".$businessPurchasedPushNoteqty."',
businessPurchasedPushNotepoints = '".$businessPurchasedPushNotepoints."',
deliveryReferralsqty = '".$deliveryReferralsqty."',
deliveryReferralspoints = '".$deliveryReferralspoints."',
deliveryItemqty = '".$deliveryItemqty."',
deliveryItempoints = '".$deliveryItempoints."',
deliverycompletedqty = '".$deliverycompletedqty."',
deliverycompletedpoints = '".$deliverycompletedpoints."',
Totalqty = '".$Totalqty."',
Totalpoint = '".$Totalpoint."',
   WHERE userName = '".$userName."'");
}else{
// Insert member data in the database
mysqli_query($con,"INSERT INTO points (
userDate, 
userName, 
userType,
 clientReferralsqty, 
 clientReferralspoints, 
 clientpurchasecreditqty,
 clientpurchasecreditpoints,
 clientpurchasecashqty,
 clientpurchasecashpoints,
 clientpurchasedifferqty,
 clientpurchasedifferpoints,
 clientpurchasedeachqty,
 clientpurchasedeachpoints,
 businessReferralsqty,
 businessReferralspoints,
 businessPurchasedMonthlyqty,
 businessPurchasedMonthlypoints,
 businessPurchasedHalfqty,
 businessPurchasedHalfpoints,
 businessPurchasedyearqty,
 businessPurchasedyearpoints,
 businessSoldqty,
 businessSoldpoints,
 businessSaleMonthqty,
 businessSaleMonthpoints,
 businessSoldCreditqty,
 businessSoldCreditpoints,
 businessPurchasedPushqty,
 businessPurchasedPushpoints,
 businessPurchasedPushNoteqty,
 businessPurchasedPushNotepoints,
 deliveryReferralsqty,
 deliveryReferralspoints,
 deliveryItemqty,
 deliveryItempoints,
 deliverycompletedqty,
 deliverycompletedpoints,
 Totalqty,
 Totalpoint) 
 VALUES (
 '".$userDate."',
 '".$userName."', 
 '".$userType."', 
 '".$clientReferralsqty."', 
 '".$clientReferralspoints."',
 '".$clientpurchasecreditqty."', 
 '".$clientpurchasecreditpoints."', 
 '".$clientpurchasecashqty."', 
 '".$clientpurchasecashpoints."', 
 '".$clientpurchasedifferqty."', 
 '".$clientpurchasedifferpoints."', 
 '".$clientpurchasedeachqty."', 
 '".$clientpurchasedeachpoints."', 
 '".$businessReferralsqty."', 
 '".$businessReferralspoints."', 
 '".$businessPurchasedMonthlyqty."', 
 '".$businessPurchasedMonthlypoints."', 
 '".$businessPurchasedHalfqty."', 
 '".$businessPurchasedHalfpoints."', 
 '".$businessPurchasedyearqty."', 
 '".$businessPurchasedyearpoints."', 
 '".$businessSoldqty."', 
 '".$businessSoldpoints."', 
 '".$businessSaleMonthqty."', 
 '".$businessSaleMonthpoints."', 
 '".$businessSoldCreditqty."', 
 '".$businessSoldCreditpoints."', 
 '".$businessPurchasedPushqty."', 
 '".$businessPurchasedPushpoints."', 
 '".$businessPurchasedPushNoteqty."', 
 '".$businessPurchasedPushNotepoints."', 
 '".$deliveryReferralsqty."', 
 '".$deliveryReferralspoints."', 
 '".$deliveryItemqty."', 
 '".$deliveryItempoints."', 
 '".$deliverycompletedqty."', 
 '".$deliverycompletedpoints."', 
 '".$Totalqty."', 
 '".$Totalpoint."'
 )");
}
}

// Close opened CSV file
fclose($csvFile);

$qstring = '?status=succ';
}else{
$qstring = '?status=err';
}
}else{
$qstring = '?status=invalid_file';
}
}

// Redirect to the listing page
header("Location:Dashboard.php".$qstring);
