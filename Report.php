<?php
include "Connection.php";

// Check user login or not
if(!isset($_SESSION['uname'])){
    header('Location: Login.php');
}

// logout
if(isset($_POST['but_logout'])){
    session_destroy();
    header('Location: Login.php');
}


$to=$_POST['to'];
$search=$_POST['search'];
$from=$_POST['from'];
?>

<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> abierTTo Admin</title> 
    <link href="https://www.smarteyeapps.com/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.smarteyeapps.com/assets/css/font-awesome.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

<style type="text/css">
.mbtable{
display:none;
}
h3,h4{
    font-family: Montserrat;
   
}
.desktable{
  display:block;
  font-family: Montserrat;
}
table{
    font-size:14px;
}
input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
  margin-left:auto;
  margin-right:auto;
  border: 1px solid #D3D3D3;
}
input[type=text]:focus {
    outline: none;
}
.container1 {
  display: block;
  position: relative;
  padding-left: 25px;
  padding-top:5px;
  cursor: pointer;
  font-size: 15px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;

  user-select: none;
}

/* Hide the browser's default checkbox */
.container1 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  
}

/* Create a custom checkbox */
.checkmark {
    margin-top:5px;
  position: absolute;
  top: 0;
  left: 0;
  height: 18px;
  width: 18px;
  border: solid 1px  #2196F3;
 border-radius: 50%;
}


/* When the checkbox is checked, add a blue background */
.container1 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container1 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container1 .checkmark:after {
  left: 4px;
  top: 2px;
  width: 7px;
  height: 11px;
  border: .5px solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
div.card {
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  
}
div.card1 {
  width: 97%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
  margin-left:10px;
  margin-top:20px;
}

.search{
  background-color: #5CC4BA;
 border-radius: 2rem; 
  border: none;
  color: white;
  padding-top:10px;
  padding-bottom:10px;
  padding-left:40px;
  padding-right:40px;
  text-decoration: none;
  margin: 4px 2px;
  font-weight:bold;
  cursor: pointer;
}
div.header {
  background-color:  #B2B2B2;
  color: black;
  padding: 8px;
  font-size: 1em;
  font-weight: bold;

}

div.container {
  padding: 0px;
  font-size: 2em;
}

.topnav {
  overflow: hidden;
  background-color: FBB040;
  height:50px;
}
.topnav1 {
  overflow: hidden;
 background-color:#DCDCDC;
  height:50px;
  font-family: Verdana,sans-serif;
}
input[type="submit"] {
border: none;
outline:none;

}
.topnav1 a {
    float: right;
  color: #444;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 14px;
  font-weight:bold;
}
.topnav1 a.active {
  color: #E31E36;
  font-weight:bold;
}
.content {
  
  position: inherit;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 50px;
}
</style>

    <script src="https://www.smarteyeapps.com/assets/js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
    <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    </head>
    <body>
    <div class="desktable">
    <div class="topnav">
 <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
<div class="topnav1" id="myHeader">
  <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40" style="padding-top:10px ;padding-left:10px">
  <a href="#news"><form method='post' action=""><input type="submit" value="Logout" name="but_logout" style="background-color:transparent;cursor:pointer" ></form></a>
  <a class="active" href="Report.php" style="text-decoration: none;">Report</a>
  <a  href="History.php" style="text-decoration: none;color:#444">History</a>
        <a  href="calculation.php" style="text-decoration: none; color:#444">Calculation</a>
        <a href="template.php" style="text-decoration: none;color:#444">Create Template</a>

  <a href="AddEntry.php" style="text-decoration: none;color:#444">Add Entry</a>
  <a  href="Dashboard.php" style="text-decoration: none;color:#444">Dashboard</a>
</div>





<?php
// Get status message
if(!empty($_GET['status'])){
switch($_GET['status']){
case 'succ':
$statusType = 'alert-success';
$statusMsg = 'Members data has been imported successfully.';
break;
case 'err':
$statusType = 'alert-danger';
$statusMsg = 'Some problem occurred, please try again.';
break;
case 'invalid_file':
$statusType = 'alert-danger';
$statusMsg = 'Please upload a valid CSV file.';
break;
default:
$statusType = '';
$statusMsg = '';
}
}
?>

<!-- Display status message -->
<?php if(!empty($statusMsg)){ ?>

<div class="col-xs-8 container">
<div class="alert <?php echo $statusType; ?>"><?php echo $statusMsg; ?></div>
</div>
<?php } ?>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<div class="card content">
<div class="header">
    <h3 style="font-weight:bolder;padding-top:10px">USER REPORT</h3>
  </div>
<div class="container">
      
<form action="" method="POST">
<table width="100%" style="font-size:14px">
<tr>
<td width="80%" style="padding-left:30px">
<input id="rgfg"  style="font-size:14px" type="text" autocomplete="off" name="search" value="<?php echo $search ?>"  placeholder="Search here" >
</td>

<td width="10%">
<a href="javascript:void(0);"  style="font-size:14px"  class="search"  onclick="formToggle('importFrm');"><i class="plus"></i> Import</a>
</td>
<td width="10%">
<!-- <form method='post' action='export.php'>

<input type="submit" name="Export" class="search" value="Export" >

</from> -->
<a href="exportData.php?from=<?=$_POST['from']?>&to=<?=$_POST['to']?>&search=<?=$_POST['search']?>"  style="font-size:14px" name="export" class="search" style="text-decoration: none;color:white"><i class="exp" name="export"></i> Export</a>
</td>
</tr>
</table>

<table width="75%" style="margin-bottom:-30px;fonr-size:14px">
<tr>
<td style="padding-left:40px">
From  <input type="date" class="it" name="from" value="<?php echo $from; ?>" style="margin-left:10px" > 
</td>
<td>
To  <input type="date" class="it" name="to" value="<?php echo $to; ?>" style="margin-left:10px">
</td>
<td>
<input type="submit" name="sub" class="search" value="Search" >

</td>
</tr>

</table>
<!-- <form method='post' action='export.php'>

<input type="submit" name="Export" class="search" value="Export" >

</from> -->
<table width="100%"  align="right" style="margin-top:20px;margin-right:20px;margin-bottom:20px">
<thead >
<tr>
<th width="80%" style="padding-left:50px">

</th>
<th width="10%"> 
</th>
<th width="10%"> 



</th>

</tr>
</thead>
</table>
<!-- CSV file upload form -->
<table  id="importFrm" style="display: none;padding-left:40px">
    <tr>
     <td>
         <form action="importData.php" method="post" enctype="multipart/form-data">
<input type="file" name="file" />
<input type="submit" class="search" name="importSubmit" value="Import">
</form>
     </td>
    </tr>
</table>

</from>
<table width="100%"  class="table table-bordered">
<thead>
  <tr style="background-color: #B2B2B2;color:black;text-align:center;font-family: 'Montserrat';font-size:14px"><th>ID</th>
<th>Date</th>
<th>User Name</th>
<th>User Type</th>
<th>No. Of Times</th>
<th>Total Points</th>

</tr>
</thead>
<tbody>
<?php

   
  
if($from!="" && $to!="" && $search!="")
{
  $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to' AND (userName='$search' OR userType LiKe'%$search%' OR ID='$search' OR Totalpoint='$search' OR Totalqty='$search')");

}else
if($from!="" && $to!="" ){
 $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to'");
}else if($search!="")
{
    $result = mysqli_query($con,"SELECT * FROM points where  userName='$search'  OR userType LiKe'%$search%' OR ID='$search'OR Totalpoint='$search' OR Totalqty='$search'");

}
else{
$result = mysqli_query($con,"SELECT * FROM points ORDER BY userDate  DESC");
}
  //  $result="select * from points where userDate BETWEEN '$from' and '$to'";
  if(mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
    ?>
    <tr >
    <td style="text-align:center"><?php echo $row['ID']; ?></td>
    <td style="text-align:center"><?php echo $row['userDate']; ?></td>
    <td ><?php echo $row['userName']; ?></td>
    <td ><?php echo $row['userType']; ?></td>
    <td style="text-align:center"><?php echo $row['Totalqty']; ?></td>
    <td style="text-align:center"><?php echo $row['Totalpoint']; ?></td>
    </tr>
    
    <?php
   }
  }
  else {?>
<tr><td colspan="6">No records found...</td></tr>
 <?php } ?>
   
  
  


<!-- else{
Get member rows
$result = mysqli_query($con,"SELECT * FROM points ORDER BY userDate DESC");
   
if(mysqli_num_rows($result) > 0){
while($row = mysqli_fetch_assoc($result)){
?>
<tr >
<td style="text-align:center"><? /*php echo $row['ID']; ?></td>
<td style="text-align:center"><?php echo $row['userDate']; ?></td>
<td ><?php echo $row['userName']; ?></td>
<td ><?php echo $row['userType']; ?></td>
<td style="text-align:center"><?php echo $row['Totalqty']; ?></td>
<td style="text-align:center"><?php echo $row['Totalpoint']; ?></td>

</tr>\ 
<?php } }else{ ?>
<tr><td colspan="5">No records found...</td></tr>
<?php } ?>
<?php } ?>*/?>*/-->
</tbody>
</table>
<div class="row container">
<!-- Import & Export link -->
<div class="col-md-12 head">
<div class="float-right">


</div>
</div>


<!-- Data list table -->
</div>

</div>
</div>
</div>
<script>
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>
<script>
// Data Picker Initialization
$(document).ready(function(){
   $("#fromDate").datepicker({
       format: 'dd-mm-yyyy',
       autoclose: true,
   }).on('changeDate', function (selected) {
       var minDate = new Date(selected.date.valueOf());
       $('#toDate').datepicker('setStartDate', minDate);
   });

   $("#toDate").datepicker({
       format: 'dd-mm-yyyy',
       autoclose: true,
   }).on('changeDate', function (selected) {
           var minDate = new Date(selected.date.valueOf());
           $('#fromDate').datepicker('setEndDate', minDate);
   });
});
</script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>


<!-- Mobile View -->
<style>
@media only screen and (max-width: 600px) {
  .mbtable{
display:block;
font-family: Montserrat;
}
.desktable{
  display:none;
}
  div.mcard {
  width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  /*margin-top:20px;*/
  /*padding-left:10px;*/
  /*padding-top:10px;*/
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
.topnav {
  overflow: hidden;
  background-color: FBB040;
  height:50px;
}

body { margin: 0 auto;}

header {


	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;
}
footer {
	height: 50px;
	background-color: #494949;
	color: #fff;
	display: flex;
	align-items: center;
	justify-content: center;
	bottom: 0;
	position: fixed;
	width: 100%;
}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
.topnav1 {
  overflow: hidden;
  background-color:rgba(242,242,242,255);
  height:50px;
}
.topnav1 a {
  color: #444;
  /* text-align: center; */
  padding: 14px 16px;
  text-decoration: none;
  font-size: 10px;
}
.topnav1 a.active {
  color: #d27c85;
  font-weight:bold;
}
div.card1 {
  width: 95%;
  text-align: center;
  margin-left:10px;
  margin-top:20px;

}
@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
body { margin: 0 auto; }

header {
	height: 60px;
background-color:B2B2B2;
	display: flex;
	align-items: center;
	justify-content: center;
}
header > h1 {
	width: calc(100% - 160px);
	text-align: center;
	font-size: 20px;
	color: white;
}
header > .top {
	position: absolute;
	left: 20px;
}
header > .top a.menu_icon i {
	color: #000;
	font-size: 40px;
	padding-top: 5px;
	transition: .2s ease;
}
header > .top a.menu_icon:hover i {
	color: #fff;
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
nav.menu > a {
    display: block;
    padding: 5px;
    margin: 15px 0 0px 20px;
    color: #494949;
    text-transform: uppercase;
}
main {
	width: 100%;
	/* padding: 30px; */
	box-sizing: border-box;

}

.menu_show {
	left: 0!important;
}

@media screen and (max-width: 425px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
	header h1 {
		font-size: 16px;
	}
}
@media screen and (max-width: 360px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
}
nav.menu {
	width: 200px;
	min-height: calc(100vh - 121px);
  background-color:rgba(242,242,242,255);
	position: absolute;
	left: -300px;
	transition: .3s all;
}
}
  .topnav a:not(:first-child), .dropdown .dropbtn {
    
  }
  .topnav1 a.icon {
    float: right;
    display: block;
  }
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

}

@media screen and (max-width: 600px) {
  div.mcard {
    width: 95%;
  text-align: center;
  margin-left:auto;
  margin-right:auto;
  margin-top:20px;
  box-shadow:0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);

}
  .fa-1x {
font-size: 1.5rem;
}
.navbar-toggler.toggler-example {
cursor: pointer;
}
.dark-blue-text {
color: #0A38F5;
}
.dark-pink-text {
color: #AC003A;
}
.dark-amber-text {
color: #ff6f00;
}
.dark-teal-text {
color: #004d40;
}

  .topnav1.responsive {position: relative;}
  .topnav1.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav1.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav1.responsive .dropdown {float: none;}
  .topnav1.responsive .dropdown-content {position: relative;}
  .topnav1.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}
}

.multiselect {
  margin-left: 200px;
    width: 100px;
    }
.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  text-align: left;
}

#checkbox_div {
  display: none;
  border: 1px #dadada solid;
}

#checkbox_div label {
  display: block;
  text-align: left;
}

#checkbox_div label:hover {
  background-color: #1e90ff;
}
.logout{
    background: transparent;
    text-transform: uppercase;
    padding-left:1px;
}

 </style>


 <div class="mbtable">
 <div class="topnav">
     

 <a href=" https://www.youtube.com/channel/UC2Qc9IlKBZkNDJVp0S1FzYg/videos" target="_blank">
    <img src="./Images/youtube.png" alt="Youtube" width="40" height="40" style="float: right;margin-top:5px;margin-right:5px"></a>
    
     <a href="https://www.linkedin.com/company/28588810/admin/" target="_blank">
    <img src="./Images/linkedin.png" alt="linkedin" width="30" height="30" style="float: right;margin-top:10px;margin-right:1px"></a>
    
     <a href="https://www.instagram.com/abiertto/" target="_blank">
    <img src="./Images/instagram1.png" alt="Instagram" width="30" height="30" style="float: right;margin-top:10px;margin-right:4px"></a>
    
    <a href="https://www.facebook.com/abiertto" target="_blank">
    <img src="./Images/facebook1.png" alt="Facebook"  width="27" height="27" style="float: right;margin-top:11px ;margin-right:5px"> </a>

</div>
 <header>
 
		<div class="top">
    <table>
    <tr>
    <td>
        <a href="#" style=" float: right;" class="menu_icon"><i style=" float: left;"  class="material-icons">dehaze</i></a>

    </td>
    <td style="padding-right:15px">
        <img src="./Images/abiertto.png" alt="Girl in a jacket" width="200" height="40"  style="margin-left:60%">


    </td>
    </tr>
    </table>

		</div>
	</header>
	<nav class="menu">
  <a  class="item_menu" href="Dashboard.php">Dashboard</a>
  <a  class="item_menu" href="AddEntry.php">Add Entry</a>
  <a  class="item_menu" href="History.php">History</a>
  <a  class="item_menu"  href="Report.php">Report</a>
  <a href="#"  class="item_menu">
  <form method='post' action="">
            <input type="submit" value="Logout" class="logout" name="but_logout">
        </form>
  </a>
	</nav>

    <div class="mcard">
             <div class="header">
    <h3>USER REPORT</h3>
  </div>
    <form action="" method="post">
        <input id="mgfg" type="text" style="width:95%;margin-bottom:20px;margin-top:10px" autocomplete="off" name="search" placeholder="Search here" >

    <table width="100%" style="padding-left:20px">
    <form action="importData.php" method="post" enctype="multipart/form-data">
    <tr  id="importFrm">
<td style="padding-left:10px">
<input type="file" name="file" />
</td>
</tr>

</form>

    </table>
    <table width="100%">
            <tr style="text-align:left">
    <td style="text-align:left">
<input type="submit" name="importSubmit"  class="search" style="width:90%;margin-top:10px;text-align:center" value="Import">
</td>
<td style="text-align:left">
    <a href="exportData.php?from=<?=$_POST['from']?>&to=<?=$_POST['to']?>&search=<?=$_POST['search']?>" class="search"  style="width:90%;margin-left:auto;margin-right:auto;display:block;margin-top:10px;text-align:center;text-decoration:none;color:white"><i class="exp"></i> Export</a> 
</td>
</tr>
    </table>
<table width="100%">
<tr height="50px">

<td width="80%" style="padding-left:50px">
From : <input type="date" class="it" name="from"  value="<?php echo $_POST['from']; ?>" style="margin-right:50px"> 
</td>
</tr>
<tr>
<td width="80%" style="padding-left:50px">

To : <input type="date"  value="<?php echo $_POST['to']; ?>" class="it" name="to" style="margin-left:15px">
</td>
</tr>
<tr>
<td width="100%">

<input type="submit" name="sub" class="search" value="Search" style="width:90%;margin-left:auto;margin-right:auto;display:block;margin-top:10px;margin-bottom:10px" >
</td>
</tr>
</table>
</div>
<!-- CSV file upload form -->

</div>
<div class="mbtable">
    <div class="mcard">
           <div class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
      <select style="margin: 4%;">
        <option> Column..</option>
      </select>
      <div class="overSelect"></div>
    </div>
    <div id="checkbox_div"  style=" text-align: left;
   
    display: none;
    position: absolute;
    background-color: snow;
    width: 27%;
">

<input type="checkbox" value="hide" id="name_col" onchange="hide_show_table(this.id);" >ID</br>
<input type="checkbox" value="hide" id="age_col" onchange="hide_show_table(this.id);" >Date</br>
<input type="checkbox" value="hide" id="city_col" onchange="hide_show_table(this.id);">Times</br>
</div>
    </div>
    
        
        
        <div style="overflow-x:auto;">
<table width="100%" class="table">
<thead class="thead-dark">
<tr style="text-align:center">
<th id="name_col_head">ID</th>
<th id="age_col_head">Date</th>
<th >User Name</th>
<th >User Type</th>
<th id="city_col_head">Times</th>
<th>Points</th>


</tr>
</thead>
<tbody>

<?php

   
  
if($from!="" && $to!="" && $search!="")
{
  $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to' AND (userName='$search' OR userType LiKe'%$search%' OR ID='$search' OR Totalpoint='$search' OR Totalqty='$search')");

}else
if($from!="" && $to!="" ){
 $result = mysqli_query($con,"SELECT * FROM points where userDate BETWEEN '$from' and '$to'");
}else if($search!="")
{
    $result = mysqli_query($con,"SELECT * FROM points where  userName='$search'  OR userType LiKe'%$search%' OR ID='$search'OR Totalpoint='$search' OR Totalqty='$search'");

}
else{
$result = mysqli_query($con,"SELECT * FROM points ORDER BY userDate  DESC");
}
  //  $result="select * from points where userDate BETWEEN '$from' and '$to'";
  if(mysqli_num_rows($result) > 0){
    while($row = mysqli_fetch_assoc($result)){
    ?>
    <tr >
    <td style="text-align:center"><?php echo $row['ID']; ?></td>
    <td style="text-align:center"><?php echo $row['userDate']; ?></td>
    <td ><?php echo $row['userName']; ?></td>
    <td ><?php echo $row['userType']; ?></td>
    <td style="text-align:center"><?php echo $row['Totalqty']; ?></td>
    <td style="text-align:center"><?php echo $row['Totalpoint']; ?></td>
    </tr>
    <?php
   }
  }
  else {?>
<tr><td colspan="6">No records found...</td></tr>
 <?php } ?>
   
  
  


<!-- else{
Get member rows
$result = mysqli_query($con,"SELECT * FROM points ORDER BY userDate DESC");
   
if(mysqli_num_rows($result) > 0){
while($row = mysqli_fetch_assoc($result)){
?>
<tr >
<td style="text-align:center"><? /*php echo $row['ID']; ?></td>
<td style="text-align:center"><?php echo $row['userDate']; ?></td>
<td ><?php echo $row['userName']; ?></td>
<td ><?php echo $row['userType']; ?></td>
<td style="text-align:center"><?php echo $row['Totalqty']; ?></td>
<td style="text-align:center"><?php echo $row['Totalpoint']; ?></td>

</tr>\ 
<?php } }else{ ?>
<tr><td colspan="5">No records found...</td></tr>
<?php } ?>
<?php } ?>*/?>*/-->
</tbody>
</table>
</div>
</div>
	</main>
</div>
</div>
<script>
  $(document).ready(function() {
	$("body").on('click', '.top', function() {
		$("nav.menu").toggleClass("menu_show");
	});
});
  </script>
  <script>
$(document).ready(function(){
    $('input[type="checkbox"]').click(function(){
        var inputValue = $(this).attr("value");
        $("." + inputValue).toggle();
    });
});
</script>
<!-- Show/hide CSV upload form -->

<script>
function formToggle(ID){
var element = document.getElementById(ID);
if(element.style.display === "none"){
element.style.display = "block";
}else{
element.style.display = "none";
}
}
</script>
<script>
            $(document).ready(function() {
                $("#rgfg").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#rgeeks tr").filter(function() {
                        $(this).toggle($(this).text()
                        .toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $("#mgfg").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#mgeeks tr").filter(function() {
                        $(this).toggle($(this).text()
                        .toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
        
           <script type="text/javascript">
function hide_show_table(col_name)
{
 var checkbox_val=document.getElementById(col_name).value;
 if(checkbox_val=="hide")
 {
  var all_col=document.getElementsByClassName(col_name);
  for(var i=0;i<all_col.length;i++)
  {
   all_col[i].style.display="none";
  }
  document.getElementById(col_name+"_head").style.display="none";
  document.getElementById(col_name).value="show";
 }
	
 else
 {
  var all_col=document.getElementsByClassName(col_name);
  for(var i=0;i<all_col.length;i++)
  {
   all_col[i].style.display="table-cell";
  }
  document.getElementById(col_name+"_head").style.display="table-cell";
  document.getElementById(col_name).value="hide";
 }
}


$(document).ready(function () {
  $("#test").CreateMultiCheckBox({ width: '230px',
             defaultText : 'Select Below', height:'250px' });
});


var expanded = false;

function showCheckboxes() {
  var checkbox_div = document.getElementById("checkbox_div");
  if (!expanded) {
    checkbox_div.style.display = "block";
    expanded = true;
  } else {
    checkbox_div.style.display = "none";
    expanded = false;
  }
}
</script>
</body></html>